import datetime
import os.path
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
import tempfile as tmp
from marvin.drone.hydrophone.spectrogram_generator import wav2spectrogram, spectrogram2xarray, XrStruct

file = Path(r'C:\data\tmp') / "SBW1597_20100101_010256.wav"
file = Path(r'C:\data\tmp') / "impact-piling_22s.wav"

import pytz
test_name = Path("SBW1597_20100101_010256.wav")
test_name= test_name.stem
test_name_list=test_name.rsplit("_")
test_name="_".join(test_name_list[1::]) #remove cardinal
starting_date=datetime.datetime.strptime(test_name,format("%Y%m%d_%H%M%S"))
starting_date=starting_date.astimezone(tz=pytz.utc)

frequencies, times, spectrogram = wav2spectrogram(filename=file, starting_date=starting_date)

xrvalue = spectrogram2xarray(times=times,frequencies= frequencies,values= spectrogram)

def plot(xrvalue):
    v = xrvalue.transpose( "frequency","time")
    fig = px.imshow( np.log(v["spectrogram"]),color_continuous_scale='RdBu_r', origin='lower')
    fig.show()

def save(xrvalue,output):
    xrvalue.to_netcdf(path=output)
    print(f"Export raw save to {output} size = {os.path.getsize(output)}")


#Export
save(xrvalue,tmp.mktemp(dir=".",suffix="_raw.nc"))

plot(xrvalue)


#Resample
resampled_value = xrvalue.resample({XrStruct.time:"100ms"}).mean()
save(resampled_value,tmp.mktemp(dir=".",suffix="_resampled.nc"))

plot(resampled_value)


#xrvalue["spectrogram"].plot()

plot(xrvalue)
#
# spectrogram = np.log(spectrogram)
# plt.pcolormesh( frequencies,times, spectrogram)
# plt.ylabel('Frequency [Hz]')
# plt.xlabel('Time [sec]')
# plt.show(block=True)
