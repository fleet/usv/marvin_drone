import os
import tempfile
from pathlib import Path

from marvin.drone.adcp.rdi import parameters


def test_serialization():
    tmp_file = tempfile.mktemp(suffix=".json")
    try:
        param1 = parameters.AgentParameters()
        parameters.write_tojson(param1, Path(tmp_file))
        read_param = parameters.read_fromjson(Path(tmp_file))

        assert param1 == read_param
    finally:
        os.remove(tmp_file)
