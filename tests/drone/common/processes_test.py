import psutil

import marvin.common.processes as proc


def test_kill():
    """
    Test kill a process
    """
    # launch two basic processes
    spawned_process = ["python", "-c", "'import time; time.sleep(1000)'"]
    p1 = psutil.Popen(spawned_process)
    p2 = psutil.Popen(spawned_process)
    proc._kill_processes([p1, p2], timeout=1)

    for p in [p1, p2]:
        assert not p.is_running()
