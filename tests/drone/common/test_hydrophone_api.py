import os
import tempfile
import datetime
from pathlib import Path

from marvin.common.hydrophone_api import MessageEncoder
from marvin.drone.hydrophone.spectrogram_generator import wav2spectrogram
from tests.drone.hydrophone import wav_dataset


def test_encode():
    test_file = tempfile.mktemp(suffix="_20100101_010256.wav")

    wav_dataset.create_wav_dataset(filename=str(test_file))
    file_path = Path(test_file)

    xr_dataset = wav2spectrogram(filename=file_path, starting_date=datetime.datetime.utcnow())
    os.remove(file_path)


    data=MessageEncoder.encode(file_name=file_path,dataset=xr_dataset)

    # nc_file = file_path.with_suffix('.nc')
    nc_buffer = xr_dataset.to_netcdf(path=None)

    file_stem,xr_decoded = MessageEncoder.decode(data)

    assert file_stem==file_path.stem
    assert xr_decoded.equals(xr_dataset)





