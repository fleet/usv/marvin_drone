import datetime
import os
import tempfile
from pathlib import Path

import pytz

import marvin.drone.hydrophone.data_processing
from tests.drone.hydrophone import wav_dataset


def test_extract_spectrogram():
    test_file = tempfile.mktemp(suffix="_20100101_010256.wav")
    wav_dataset.create_wav_dataset(filename=test_file)
    spec = marvin.drone.hydrophone.data_processing.extract_spectrogram(file_path=Path(test_file))
    assert spec is not None
    os.remove(test_file)


def test__extract_file_name_starting_date():
    date = marvin.drone.hydrophone.data_processing.extract_file_name_starting_date(
        "c:/home/SBW1597_20100101_010256.wav")
    assert date == datetime.datetime(year=2010, month=1, day=1, hour=1, minute=2, second=56, tzinfo=pytz.UTC)
