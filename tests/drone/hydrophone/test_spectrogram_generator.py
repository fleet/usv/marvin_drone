import datetime
import os
import tempfile
from pathlib import Path

from marvin.drone.hydrophone.spectrogram_generator import save_figure, wav2spectrogram
from tests.drone.hydrophone import wav_dataset


def test_save_figure():
    test_file = tempfile.mktemp(suffix="_20100101_010256.wav")

    wav_dataset.create_wav_dataset(filename=str(test_file))
    file_path = Path(test_file)

    xr_dataset = wav2spectrogram(filename=file_path, starting_date=datetime.datetime.utcnow())
    os.remove(file_path)
    # save data to disk and/or publish data to land
    figure_file = file_path.parent / (file_path.name + ".png")
    save_figure(spectogram=xr_dataset, file_name=figure_file)
    assert figure_file.exists()
    os.remove(figure_file)
