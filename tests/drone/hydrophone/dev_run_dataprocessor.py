from marvin.drone.hydrophone.data_processing import run_as_process, DataProcessor
from marvin.drone.hydrophone.data_processing_parameters import default_parameters

instance = DataProcessor(default_parameters)
instance.run()
