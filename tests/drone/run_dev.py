from pathlib import Path

from marvin.drone.adcp.rdi.rdi_agent import RdiAgent


def run_something():
    """Try to start and stop adcp"""
    working_directory = Path.cwd().parent.parent / "working_directory"

    agent = RdiAgent(working_directory=working_directory)
    print(f"Status {agent.status}")
    # agent.start()
    print(f"Status {agent.status}")
    agent.stop()
    print(f"Status {agent.status}")


if __name__ == "__main__":
    run_something()
