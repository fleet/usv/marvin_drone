#!/usr/bin/env python
import time
from pathlib import Path
from threading import Thread

import pika
import sys

from marvin.common.common_logger import configure_console
from marvin.common.hydrophone_api import MessageBroker

# Stuf for data producer mock
from marvin.land.server.context import Context
from marvin.land.server.hydrophone.hydrophone_processor import HydroMessageConsumer, MessageDecoder, HydroProcessor


def __create_connection():
    credentials = pika.credentials.PlainCredentials(username="marvin", password="marvin")
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=MessageBroker.broker_host, credentials=credentials))

    channel = connection.channel()

    channel.exchange_declare(exchange=MessageBroker.exchange_name, exchange_type=MessageBroker.exchange_type)
    return connection,channel

def __publish(channel,message):
    """create data producer"""
    channel.basic_publish(exchange=MessageBroker.exchange_name, routing_key='', body=message)
    print(f"-> Sent message size={len(message)}")

def __close_connection(connection):
    connection.close()

def _fake_producer():
    """start a process, publish two fake messages and exit"""
    #init producer
    message = "bonjour".encode('utf8')
    connection,channel = __create_connection()
    time.sleep(2)

    __publish(channel=channel,message=message)
    __publish(channel=channel,message=message)
    __close_connection(connection)

class MessageDecoderTest(MessageDecoder):
        def __init__(self,expected_message):
            self.set_message(expected_message)
            self.count= 0
        def set_message(self,expected_message):
            self.expect = expected_message

        def decode(self, message):
            print(f"hydrophone message queue : message received size={len(message)}")
            assert self.expect==message
            self.count += 1
            #break
            if self.count == 2:
                #only 2 messages
                raise MyExcept()

class MyExcept(Exception):
    ...

def test_simple_connection():
    _fake_producer()


def test_periodic_process():
    """test periodic process message exchange"""
    configure_console()

    #create data processor


    message = "bonjour".encode('utf8')

    decoder= MessageDecoderTest(message)

    consumer = HydroMessageConsumer(decoder = decoder)

    thread = Thread(
        target=_fake_producer
    )
    thread.start()
    try:

        consumer.consume()
    except MyExcept:
        #we got 2 messages, exiting
        pass

    assert decoder.count == 2


def test_HydroProcessor():
    context = Context(working_dir=Path(r"C:\dev\workspaces\marvin_drone\working_directory"))
    message = "bonjour".encode('utf8')

    decoder= MessageDecoderTest(message)

    thread = Thread(
        target=_fake_producer
    )
    thread.start()

    p = HydroProcessor(context=context)

    try:
        p.start()
    except MyExcept:
        #we got 2 messages, exiting
        pass
