"""send commands to EK80"""
from marvin.land.model.data_model import EK80Controler
from marvin.land.parameters import LandParameters


def test_switch_to_cw_mode():
    parameters=LandParameters()
    parameters.drix_ip_adress="134.246.149.43"
    controler=EK80Controler(parameters)
    controler.direct_cw()
