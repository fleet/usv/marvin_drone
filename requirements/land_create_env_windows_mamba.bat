REM install mamba if not already done
call conda install mamba -c conda-forge -y

echo mamba env remove --name pydrone -y
call mamba env remove --name pydrone -y

echo mamba env create -f requirements_land.yml --name pydrone
call mamba env create -f requirements_land.yml --name pydrone

