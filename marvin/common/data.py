from datetime import datetime
from enum import Enum, IntEnum

from pydantic import BaseModel #pylint: disable=no-name-in-module


class DataStatusEnum(IntEnum):
    UNKNOWN = -1
    KO = 0
    OK = 1


class DataStatus(BaseModel):
    """Pydantic simple data status class"""
    status: DataStatusEnum
    last_modified_date: datetime
    file_path: str = ""
