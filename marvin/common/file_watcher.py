import dataclasses
import datetime
import time
import traceback
from enum import IntEnum, auto, Enum
from multiprocessing import Process, Queue
from pathlib import Path
from typing import List, Protocol, Callable, Dict, Any, Tuple
import logging


# @dataclasses
# class FileEntry:
#     name: str
#     time: float
import pytz

from marvin.common.data import DataStatusEnum, DataStatus


class FileEvent:
    class EventType(Enum):
        NEW = "New"
        MODIFIED = "Modified"
        # DELETED

    def __init__(self, path: Path, st_mtime: float, event_type: EventType):
        self.path = path
        self.modification_time = datetime.datetime.fromtimestamp(st_mtime, tz=pytz.utc)
        self.type = event_type

    path: Path
    modification_time: datetime


class DirectoryWatchPoller:
    """
    Poll directory to check for file changes in it
    File changed are monitored
    """

    def __init__(
        self,
        directories: List[Path],
        filter_pattern: List[str],
        period_s: float = 10,
        logger=logging.getLogger("DirectoryWatchPoller"),
    ):
        """
        Filter is a list of file pattern
        """
        self.directories = directories
        self.period_s = period_s
        self.filter = filter_pattern
        self._stop = False
        self.logger = logger

    @property
    def stop(self):
        return self._stop

    @stop.setter
    def stop(self, value):
        self._stop = value

    def watch(self, notify_func: Callable[[FileEvent], None]):
        """
        Start a loop on a directory, files not already seen or having been changed will raise an event
        If any error occurs it is up to the calling program to treat exception and call for a new watch
        """
        file_histories = {directory:{} for directory in self.directories} #TODO CHANGE HISTORY TO HAVE ONE HISTORY PER DIRECTORY

        # a dictionnary containing file basename and their modification time
        # try:
        while not self.stop: #pylint: disable =too-many-nested-blocks
            # first of all check for pending files
            # try:
            start = time.time()
            for pattern in self.filter:
                for directory in self.directories:
                    files = directory.glob(pattern=pattern)
                    file_history = file_histories[directory]
                    #WE ASSUME THAT FILES IN BOTH DIRECTORIES HAVE DIFFERENTS NAMES WHICH IS GOOD ENOUGH FOR DELMOGES
                    for file in files:
                        modification_time = file.stat().st_mtime
                        if file not in file_history:
                            notify_func(
                                FileEvent(
                                    path=file,
                                    st_mtime=modification_time,
                                    event_type=FileEvent.EventType.NEW,
                                )
                            )
                        else:
                            last_mod = file_history[file]
                            if last_mod != modification_time:
                                notify_func(
                                    FileEvent(
                                        path=file,
                                        st_mtime=modification_time,
                                        event_type=FileEvent.EventType.MODIFIED,
                                    )
                                )
                        file_history[file] = modification_time
            # Do not process exception, it's up to the calling program to treat errors

            # except Exception as e:
            #      # empty queue continue processing
            #     self.logger.error(f"Error while parsing file : {e}")
            #     traceback.print_exc()

            elapsed = time.time() - start
            wait_time = self.period_s - elapsed
            if wait_time > 0:
                time.sleep(wait_time)


def _watchAsProcess(queue: Queue, watcher_args):
    """
    Wrapper function calling directory watcher
    """

    # push event to queue
    def notify(event: FileEvent):
        queue.put(event)

    watcher = DirectoryWatchPoller(**watcher_args)
    watcher.watch(notify)


def directory_watch_mp(**kwargs) -> Tuple[Process, Queue]:
    """
    Watch a directory as a multi process, file events are written to a queue
    return a tuple containing the Process and the Queue for file events
    """
    wqueue = Queue()
    p = Process(target=_watchAsProcess, args=(wqueue, kwargs))
    p.start()
    return p, wqueue
    # for v in queue:
    #
    # p.join()


class DataStatusMonitor:
    """
    A directory watcher launched as an independant process
    """

    data_status: DataStatus

    def __init__(self, **kwargs):
        """Create a directory watcher, parameters are identicals to DirectoryWatchPoller"""
        self.process_datamonitor, self.data_monitor_queue = directory_watch_mp(**kwargs)
        self.data_status = DataStatus(
            status=DataStatusEnum.UNKNOWN,
            last_modified_date=datetime.datetime(
                year=datetime.MINYEAR, month=1, day=1, tzinfo=pytz.utc
            ),
        )

    def compute_datastatus(self, datastatus_timeout: float):
        # retrieve data status
        # empty file monitoring qeue
        last_mod_time = None  # most recent data modification time
        last_file_path = None
        queue = self.data_monitor_queue
        # poll the queue to retrieve the most recent event (ie file modification)
        while not queue.empty():
            event = queue.get_nowait()
            if event is not None:
                mod_time = event.modification_time
                filepath = event.path
                if (
                    last_mod_time is None or mod_time > last_mod_time
                ):  # keep most recent modification time
                    last_mod_time = mod_time
                    last_file_path = filepath

        # now check with previous status

        if (
            last_mod_time is not None
            and self.data_status.last_modified_date < last_mod_time
        ):
            self.data_status.last_modified_date = last_mod_time
            self.data_status.file_path = str(last_file_path.name)

        if self.data_status.last_modified_date < datetime.datetime.now(
            tz=pytz.utc
        ) - datetime.timedelta(seconds=datastatus_timeout):
            # if no file has been modified since datastatus_timeout
            self.data_status.status = DataStatusEnum.KO
        else:
            self.data_status.status = DataStatusEnum.OK


if __name__ == "__main__":
    # # test for development only
    ####################
    # Start in main loop

    # watcher = DirectoryWatchPoller(
    #     directory=[Path(r"C:\data\datasets\GriddedData\Drix\Gridded"), filter=["*"],
    #     wait_time=10
    # )
    #
    #
    # def notify(event: FileEvent):
    #     print(f'Event {event.event_type} on file {event.path}')
    #
    # watcher.watch(notify_func=notify)

    #########################
    # start as multi process

    process, process_queue = directory_watch_mp(
        directories= [Path(r"C:\data\datasets\GriddedData\Drix\Gridded\A"),Path(r"C:\data\datasets\GriddedData\Drix\Gridded\B")],
        filter_pattern=["*"],
        period_s=10,
    )
    i = 0
    while True:
        # empty the queues

        while not process_queue.empty():
            item = process_queue.get()
            print(f"Event {item.type} on file {item.path}")

        time.sleep(2)
        i = i + 1
