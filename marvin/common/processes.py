import subprocess
from multiprocessing import Process
from typing import List

import psutil


def kill_process(name: str, timeout=3):
    """
    kill all processes matching the given name
    """
    processes = [p for p in psutil.process_iter() if p.name().lower() == name.lower()]
    if len(processes) > 0:
        _kill_processes(processes, timeout=timeout)


def _kill_processes(processes: List[psutil.Process], timeout):
    # ask for process termination
    for process in processes:
        process.terminate()

    # definititely kill them
    gone, alive = psutil.wait_procs(processes, timeout=timeout)
    for p in alive:
        p.kill()


def get_processes_by_name(name: str) -> List[psutil.Process]:
    """Retrieve the running processes on computer for a given name"""
    return [p for p in psutil.process_iter() if p.name().lower() == name.lower()]


def start_multiprocess(target, args) -> int:
    """simple wrapper to multiprocess"""
    p = Process(target=target, args=args)
    p.start()
    p.join()
    return p.exitcode


def spawn_process(target: str, args: List[str], shell=False) -> int:
    """spawn a new process and return its id"""
    process = subprocess.Popen([target] + args,shell=shell)  # pylint: disable = consider-using-with
    return process.pid
