import logging
import logging.handlers as hd
from pathlib import Path

FORMAT = "%(asctime)s %(message)s"


def configure_console(level=logging.INFO):
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(FORMAT))
    logger = logging.getLogger()  # retrieve default logger
    logger.setLevel(level)
    return logger


def configure(prefix: str, log_directory: Path, logger_name: str = None, level = logging.INFO):
    """Configure default logger and add a file handler"""
    if not ".log" in prefix:
        prefix = prefix + ".log"

    file = log_directory / prefix
    handler = hd.TimedRotatingFileHandler(filename=file, when="D")
    handler.setFormatter(logging.Formatter(FORMAT))
    handler.namer = lambda name: name.replace(".log", "") + ".log"
    logger = logging.getLogger(logger_name)  # retrieve default logger
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger
