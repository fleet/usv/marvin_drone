"""File containing the api paths used by client and serveur"""

# Basic agent id
from datetime import datetime
from pathlib import Path
from typing import Dict, Optional, List

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatus

supervisor_root = "/"
supervisor_agent_list = "/agents"
supervisor_agent_status = "/agents/all_status"


# structure definition for json returned by api
class AgentStatusDesc(BaseModel):
    name: str
    status: AgentStatus
    status_time: datetime
    data_status: Optional[DataStatus]


class AgentList(BaseModel):
    agents: List[str]


class AgentStatusList(BaseModel):
    agents: Dict[str, AgentStatusDesc]

class ADCPRDIConfig(BaseModel):
    config_files: Dict[str,Path]

supervisor_agent_desc = "/agents/{agent_id}"
supervisor_agent_start = "/agents/{agent_id}/start"
supervisor_agent_stop = "/agents/{agent_id}/stop"


def compute_agent_api(route: str, agent_id: str) -> str:
    """
    Compute an agent path api by replacing {agent_id}
    ex compute_agent_api("/agents/{agent_id}/start","rdi_agent") returns "/agents/rdi_agent/start"
    """
    return route.replace("{agent_id}", agent_id)


# Agents id


rdi_agent_id = "rdi_agent"
rdi_config_id = f"config_file_list"
rdi_config_fullpath=f"{rdi_agent_id}/{rdi_config_id}"
rdi_load_config = f"load_config"
rdi_load_config_fullpath=f"{rdi_agent_id}/{rdi_load_config}"

class ConfigParameter(BaseModel):
    configuration_name:str
