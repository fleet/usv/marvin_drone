"""Hydrophone API parameters

"""
import pickle
from pathlib import Path
from typing import Tuple

import xarray as xr

from marvin.drone.hydrophone.spectrogram_generator import XrStruct
from marvin.land import parameters


class MessageBroker:
    exchange_name = "hydrophone"
    exchange_type = "fanout"
    queue_name="" #only used by consumer
    broker_host = parameters.LandParameters().land_server


class TooBigException(Exception):
    """Raised when a message if too big"""

class MessageEncoder:
    MAX_MESSAGE_LEN = 2**20 #1Mbs

    @staticmethod
    def encode(file_name:Path, dataset:xr.Dataset) -> bytes:
        """Encode a Hydrophone spectrogram dataset to an broker message"""
        source = Path(file_name).stem
        dataset.attrs[XrStruct.source_att] = source
        #buffer = pickle.dumps(dataset, protocol=-1)
        buffer = dataset.to_netcdf(path=None)
        if len(buffer) > MessageEncoder.MAX_MESSAGE_LEN:
            raise TooBigException(f"dataset for {file_name} is too big size({len(buffer)}>{MessageEncoder.MAX_MESSAGE_LEN}")
        return buffer

    @staticmethod
    def decode( buffer:bytes) -> Tuple[str, xr.Dataset]:
        #data=pickle.loads(buffer)
        data = xr.load_dataset(buffer)
        file_stem = data.attrs[XrStruct.source_att]
        return file_stem,data
