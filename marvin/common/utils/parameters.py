import json
from os import PathLike
from pathlib import Path

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel  # pylint: disable=no-name-in-module


def pretty_json(text: str) -> str:
    """Prettyfy json"""
    return json.dumps(text, indent=4)


def write_tojson(config: BaseModel, filename: PathLike):
    """Write configuration to a pretty json file"""
    with open(filename, "w", encoding="utf8") as outfile:
        json_compatible_item_data = jsonable_encoder(config)
        t = json.dumps(json_compatible_item_data, indent=4)
        outfile.write(t)
