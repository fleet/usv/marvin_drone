import datetime
import json

from marvin.common.agent.agent_status import AgentStatus
from marvin.common.api_path import AgentStatusList, AgentStatusDesc


def test_serial():
    list_agents = AgentStatusList(
        agents={
            "a": AgentStatusDesc(
                name="test",
                status=AgentStatus.STOPPED,
                status_time=datetime.datetime.now(),
            )
        }
    )
    serialized = list_agents.json()
    v = json.loads(serialized)
    list_2 = AgentStatusList.parse_obj(v)
    print(list_2)
