# Agent list
from pathlib import Path


class SupervisorParameters:

    # refresh period for supervisor
    refresh_period = 10

# process monitor retention period
class ProcessMonitorParameter:
    cache_timeout = 10
