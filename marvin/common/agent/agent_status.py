from enum import IntEnum


class AgentStatus(IntEnum):
    STOPPED = 0
    FAILURE = -1
    STARTING = 1
    RUNNING = 2
