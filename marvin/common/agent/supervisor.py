import time
from datetime import datetime, timezone
from threading import Thread
from typing import Dict

import prometheus_client
from fastapi import APIRouter, HTTPException
from fastapi.responses import PlainTextResponse, Response
from prometheus_client import (
    CONTENT_TYPE_LATEST,
    CollectorRegistry,
    Enum,
    Gauge,
    Summary,
    generate_latest,
)

from marvin.common import api_path as api
from marvin.common.agent.agent import Agent
from marvin.common.api_path import AgentStatusDesc, AgentStatusList
from marvin.drone.system.services.services import Services

from .parameters import SupervisorParameters


class Supervisor:
    """
    Supervisor of the system agents. Supervisor will ensure :
        <p> agent startup and synchronisation
        <p> agent monitoring and restart
    """

    def __init__(self, agent_list=Dict[str, Agent]):
        self.starting_date = datetime.now(timezone.utc)
        self.services = Services()
        self.thread = Thread(target=self._compute)
        self._should_stop = False
        self.parameters = SupervisorParameters()
        self.agent_list = agent_list

        # setup agent and declare openmetric metrics

        self.declare_openmetrics()

    def declare_openmetrics(self):
        # remove default metrics
        prometheus_client.REGISTRY.unregister(prometheus_client.GC_COLLECTOR)
        prometheus_client.REGISTRY.unregister(prometheus_client.PLATFORM_COLLECTOR)
        prometheus_client.REGISTRY.unregister(prometheus_client.PROCESS_COLLECTOR)

        self.agent_status_metrics = {}
        self.agent_data_metrics = {}

        for agent_id in self.agent_list.keys():
            # agent_status = Enum(f'agent_{agent_id}_state', 'State of agent',
            #          states=[k.name for k in AgentStatus])
            # data_enum = Enum(f'agent_{agent_id}_data_production_state', 'Data production of agent',
            #          states=[k.name for k in DataStatusEnum])
            self.agent_status_metrics[agent_id] = Gauge(
                f"agent_{agent_id}_state", "State of agent"
            )
            self.agent_data_metrics[agent_id] = Gauge(
                f"agent_{agent_id}_data_production_state", "Data production of agent"
            )

    def _compute(self):
        """Run processing for all threads"""
        while not self._should_stop:
            start = time.time()
            agents = self.get_agent_list()
            for agent, controler in agents.items():
                controler.compute_model(services=self.services)
            elapsed = time.time() - start
            wait_time = self.parameters.refresh_period - elapsed
            if wait_time > 0:
                time.sleep(wait_time)

    def start_monitoring(self):
        self._should_stop = False
        self.thread.start()

    def stop(self):
        self._should_stop = True
        self.thread.join(timeout=10)

    def get_agent_list(self) -> Dict[str, Agent]:
        return self.agent_list

    def declare_api(self, app):
        router = APIRouter()
        supervisor = self

        @router.get(api.supervisor_root)
        def get_root():
            """
            starting date time of supervisor"
            """
            return {"Start time": supervisor.starting_date}

        @router.get(api.supervisor_agent_list)
        def get_agents_list():
            """
            Retrieve the list of known agents
            """
            return {"agents": list(supervisor.get_agent_list().keys())}

        @router.get(api.supervisor_agent_status)
        def get_agents_status():
            """
            Return the list of agent status with their id, name, status and the time stamp of the last status computation
            """
            agents = supervisor.get_agent_list()
            agents = {
                k: AgentStatusDesc(
                    name=v.name,
                    status=v.status,
                    status_time=v.get_status_time_stamp(),
                    data_status=v.data_status,
                )
                for k, v in agents.items()
            }
            return AgentStatusList(agents=agents)

        @router.get(api.supervisor_agent_desc)
        def get_agent(agent_id: str):
            """
            Retrieve agent information
            """
            agents = supervisor.get_agent_list()
            if agent_id not in agents:
                raise HTTPException(status_code=404, detail="agent not found")
            return agents[agent_id]

        @router.put(api.supervisor_agent_start)
        def start_agent(agent_id: str):
            """Start agent"""
            agents = supervisor.get_agent_list()
            if agent_id not in agents:
                self.services.logger.warning(f"Can not start agent {agent_id} : unknown")
                raise HTTPException(status_code=404, detail="agent not found")
            try:
                agents[agent_id].start()
            except Exception as e:
                raise HTTPException(
                    status_code=400, detail=f"An exception occured : {e}"
                ) from e

        @router.put(api.supervisor_agent_stop)
        def stop_agent(agent_id: str):
            """Stop agent"""
            agents = supervisor.get_agent_list()
            if agent_id not in agents:
                raise HTTPException(status_code=404, detail="agent not found")
            try:
                agents[agent_id].stop()
            except Exception as e:
                raise HTTPException(
                    status_code=400, detail=f"An exception occured : {e}"
                ) from e

        # Add openmetrics API
        @router.get("/metrics")
        def metrics():
            # s = Summary('request_latency_seconds', 'Description of summary')
            # s.observe(4.7)
            # s = Summary('test_for_pr', 'Description of summary')
            # s.observe(7)

            # update all metrics
            for agent_id, v in self.agent_list.items():
                status = v.status
                data_status = v.data_status
                # self.agent_status_metrics[agent_id].state(status.name)
                self.agent_status_metrics[agent_id].set(status.value)
                if data_status is not None:
                    self.agent_data_metrics[agent_id].set(data_status.status.value)
            data = generate_latest(prometheus_client.REGISTRY)
            res = PlainTextResponse(content=data)
            # res = Response(content=data)
            res.headers["Content-Type"] = CONTENT_TYPE_LATEST
            return res

        # END ROUTE DECLARATION

        # now really add route to app
        app.include_router(router)

        # declare agent api
        for agent_id in self.get_agent_list():
            self.get_agent_list()[agent_id].declare_api(app)
