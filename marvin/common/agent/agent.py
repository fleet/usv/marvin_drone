import abc
from datetime import datetime

from fastapi import FastAPI

from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatus
from marvin.drone.system.services.services import Services

class AgentIds:
    """Container for the list of agent ids"""
    adcp_drone = "RDI Agent (Vmdas)"


class Agent:
    """
    Base contract definition for an agent supervised by the system supervisor
    Each agent is a process or a set of processes
    """

    @property
    def status(self):
        return self._status

    def get_status(self) -> AgentStatus:
        """
        Indicate if agent is running
        """

    @status.setter
    def status(self, value):
        self._status = value
        self._status_time_stamp = datetime.utcnow()

    def get_status_time_stamp(self):
        """Return the status timestamp, ie datetime when timestamp was updated"""
        return self._status_time_stamp

    data_status: DataStatus

    def __init__(self, agent_id: str, name: str):
        self.name = name
        self.agent_id = agent_id
        self.status = AgentStatus.STOPPED
        self.data_status = None

    def start(self):
        """start agent as a new process,
        if agent have a configuration it's up to the agent to load the last known configuration
        Starting could be a long operation and should be executed in a separated thread
        """

    @abc.abstractmethod
    def compute_model(self, services: Services):
        """
        Function called periodically in the main loop to ensure heavy weight status retrieval and computations
        """

    def stop(self):
        """Stop agent execution"""

    def declare_api(self, app: FastAPI):
        """declare agent specific rest API"""
