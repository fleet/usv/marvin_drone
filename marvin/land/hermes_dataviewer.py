from dash import Dash
from marvin.land import style


from marvin.land.components.hermes import layout
from marvin.land.components.hermes.controler import HermesControler
from marvin.land.model.data_model import DataControler



def main() -> None:
    app = Dash(external_stylesheets=[style.theme])
    app.title = "Hermes dashboard"
    controler = HermesControler()
    # controler.start()
    app.layout = layout.create_layout(app, controler)
    app.run(port = 8051)
    # controler.stop()


if __name__ == "__main__":
    main()
