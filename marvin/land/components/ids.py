"""Store id used by callback and app"""
DASH_REFRESH = "refresh"


STATUS_TABLE_ID = "status-table-id"

AGENT_HISTORY_ID = "agent-history-id"


CONNEXION_STATUS_ID = "connexion-status-id"

# Agent control
START_AGENT = "btn-start-agent"
STOP_AGENT = "btn-stop-agent"

AGENT_DROP_DOWN_ID = "agent-drop-down-id"

AGENT_SPECIFIC_COMMAND_IDS = "agent-specific-commands-ids"


# Agent control
START_EK80_PING = "btn-ek80-start-pinging"
STOP_EK80_PING = "btn-ek80-stop-pinging"
EK80_TO_CW = "btn-ek80-cw-mode"
EK80_MESSAGE = "message-ek80-component"


ADCP_CONFIG_DROPDOWN = "adcp-config-drop-down-id"
ADCP_SEND_CONFIG_BTN = "adcp-send-config-button-id"
ADCP_SEND_CONFIG_LOG = "adcp-send-config-result-id"
