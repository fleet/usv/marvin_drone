from dash import Dash, html, dcc, Output
import dash_bootstrap_components as dbc

from marvin.land.model.data_model import DataControler

from . import agent_status, ids, agent_command, technical_history, ek80_command, rdi_config_sender


def create_layout(app: Dash, controler: DataControler) -> html.Div:

    component = html.Div(
        [
            html.Div(
                className="app-div",
                children=dbc.Container(
                    [
                        agent_status.render(app=app, data_controler=controler),
                        agent_command.render(app=app, data_controler=controler),
                        ek80_command.render(app=app, data_controler=controler),
                        rdi_config_sender.render(app=app,data_controler=controler),
                        technical_history.render(app=app, data_controler=controler),
                    ]
                ),
            ),
            dcc.Interval(
                id=ids.DASH_REFRESH,
                interval=controler.parameter.refresh_rate * 1000,  # in milliseconds
                n_intervals=0,
            ),
        ]
    )

    return component
