import dash_bootstrap_components as dbc
from dash import Dash, html, dash_table, Output, Input

from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatusEnum
from marvin.land import style
from marvin.land.components import ids
from marvin.land.model.data_model import (
    DataControler,
    AgentStatusModel,
    EnumConnectionStatus,
)


def render(app: Dash, data_controler: DataControler) -> html.Div:
    # call back

    def update_connexion_status():
        status = data_controler.datamodel.connexion_status.status
        time = data_controler.datamodel.connexion_status.update_time
        match status:
            case EnumConnectionStatus.SUCCESS:

                return (
                    dbc.Alert(
                        [
                            html.I(className="bi bi-check-circle-fill me-2"),
                            f"Successfull connexion with drone since {time}",
                        ],
                        color="success",
                        className="d-flex align-items-center",
                    ),
                )
            case EnumConnectionStatus.UNKNOWN:

                return dbc.Alert(
                    [
                        html.I(className="bi bi-exclamation-triangle-fill me-2"),
                        f"Unknown connexion status since {time}",
                    ],
                    color="warning",
                    className="d-flex align-items-center",
                )
            case EnumConnectionStatus.ERROR:
                return dbc.Alert(
                    [
                        html.I(className="bi bi-x-octagon-fill me-2"),
                        f"Connection to '{data_controler.datamodel.connexion_status._supervisor.name}' failed since {time}",
                    ],
                    color="danger",
                    className="d-flex align-items-center",
                )

    def update_table():
        # TODO do not refresh here
        # Should be done in another thread/process
        data = data_controler.datamodel.agent_status.as_pandas()
        # map status and data status to a human readable value
        mapping = {k.value: k.name for k in AgentStatus}
        data[AgentStatusModel.STATUS_COL] = data[AgentStatusModel.STATUS_COL].map(
            mapping
        )
        mapping = {k.value: k.name for k in DataStatusEnum}
        data[AgentStatusModel.DATA_COL] = data[AgentStatusModel.DATA_COL].map(mapping)
        dict_data = data.to_dict("records")

        # utility function to compute tooltip
        def tooltip_from_row(row):
            file = row[AgentStatusModel.LAST_FILE]
            last_modified_date = row[AgentStatusModel.LAST_FILE_TIME]
            if file is None:
                return "Unknown"
            else:
                return (
                    f"*Data production status* \n- File = {file}"
                    + "\n"
                    + "- last update date ="
                    + str(last_modified_date)
                )

        # Add a tooltip only for DATA_COL
        tooltips = [
            {
                column: {
                    "value": ""
                    if column != AgentStatusModel.DATA_COL
                    else tooltip_from_row(row),
                    "type": "markdown",
                }
                for column, value in row.items()
            }
            for row in dict_data
        ]
        table = dash_table.DataTable(
            data=dict_data,
            sort_action="native",
            columns=[
                {
                    "name": AgentStatusModel.AGENT_COL,
                    "id": AgentStatusModel.AGENT_COL,
                    "event_type": "text",
                    "editable": False,
                },
                {
                    "name": AgentStatusModel.STATUS_COL,
                    "id": AgentStatusModel.STATUS_COL,
                    "event_type": "text",
                    "editable": False,
                },
                {
                    "name": AgentStatusModel.TIME_COL,
                    "id": AgentStatusModel.TIME_COL,
                    "event_type": "datetime",
                    "editable": False,
                },
                {
                    "name": AgentStatusModel.DATA_COL,
                    "id": AgentStatusModel.DATA_COL,
                    "event_type": "text",
                    "editable": False,
                },
            ],
            tooltip_data=tooltips,
            editable=False,
            cell_selectable=False,
            style_header=style.style_header,
            style_data=style.style_data,
            style_cell_conditional=style.style_cell_conditional,
            style_data_conditional=style.style_data_conditional,
        )
        return table

    @app.callback(
        Output(ids.STATUS_TABLE_ID, "children"), Input(ids.DASH_REFRESH, "n_intervals")
    )
    def periodic_refresh_agent_status(n):  # pylint: disable=unused-argument
        return update_table()

    @app.callback(
        Output(ids.CONNEXION_STATUS_ID, "children"),
        Input(ids.DASH_REFRESH, "n_intervals"),
    )
    def periodic_refresh_connexion(n):  # pylint: disable=unused-argument
        return update_connexion_status()

    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("Drix connexion status"),
                            html.Div(
                                id=ids.CONNEXION_STATUS_ID,
                                children=update_connexion_status(),
                            ),
                        ]
                    )
                ]
            ),
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("Agent agent_status"),
                            dbc.Row(
                                [
                                    html.Div(
                                        id=ids.STATUS_TABLE_ID, children=update_table()
                                    )
                                ]
                            ),
                        ]
                    )
                ]
            ),
        ]
    )
