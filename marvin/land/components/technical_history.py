import dash_bootstrap_components as dbc
from dash import Dash, html, dash_table, Output, Input

from marvin.land.components import ids
from marvin.land.model.data_model import DataControler


def render(app: Dash, data_controler: DataControler) -> html.Div: #pylint: disable=unused-argument
    # graphana link
    g_dashboard = "http://localhost:3000/d/Kdh0OoSGz/drone-survey-monitoring?orgId=1&from=now-24h&to=now&kiosk=tv"

    # call back
    def get_data():
        return html.Div("No data")

    @app.callback(
        Output(ids.AGENT_HISTORY_ID, "children"), Input(ids.DASH_REFRESH, "n_intervals")
    )
    def periodic_refresh_agent_status(n):  # pylint: disable=unused-argument
        return get_data()

    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            dbc.Accordion(
                                [
                                    dbc.AccordionItem(
                                        html.Div(
                                            children=[
                                                html.A(
                                                    "Grafana dashboard : Open detached",
                                                    href=g_dashboard,
                                                    target="_blank",
                                                ),
                                                dbc.Row(
                                                    [
                                                        html.Iframe(
                                                            src=g_dashboard, height=800
                                                        )
                                                    ]
                                                ),
                                            ]
                                        ),
                                        title="Technical Monitoring",
                                    )
                                ],
                                start_collapsed=True,
                            )
                        ]
                    )
                ]
            ),
        ]
    )
