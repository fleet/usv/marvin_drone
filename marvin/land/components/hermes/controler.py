from pathlib import Path
from typing import List, Optional

import numpy as np
from numpy import sort

from marvin.land.parameters import HermesParameters
from marvin.land.server.ek80.sampler_parser import SampleParser


class HermesControler:
    def __init__(self):
        self.parameters = HermesParameters()
        self.input_dir = self.parameters.input_dir
        if not self.input_dir.exists():
            raise FileNotFoundError(f"input dir {self.input_dir} does not exists")
        self.selected_file: Optional[SampleParser] = None

    def remove_selection(self):
        self.selected_file = None

    def set_selection(self, file_path: Path):
        """Load data file and start periodic data retrieval"""
        if self.selected_file is not None and self.selected_file.file_name == file_path:
            return
        self.selected_file = SampleParser(
            file_name=file_path,
            time_span_seconds=self.parameters.time_span_hour * 3600
            + self.parameters.time_span_min * 60,
        )

    def get_file_list(self) -> List[Path]:
        """Parse directory to retrieve available files"""
        file_list = []
        for input_file in self.input_dir.iterdir():
            if input_file.is_file() and input_file.suffix == ".csv":
                # found a csv file in directory
                file_list.append(input_file)
        return file_list

    def set_selected_file(self, value: Path = None):
        if value is None:
            self.remove_selection()
        # retrieve file_path from name
        self.set_selection(value)

    def get_transducer_list(self):
        if self.selected_file is None:
            return []
        return self.selected_file.get_transducer_list()

    def get_time_values(self):
        if self.selected_file is None:
            return None
        return self.selected_file.get_time_values()

    def get_data(self, transducer_name):
        return self.selected_file.get_transducer_data(name=transducer_name)

    def get_data_times(self):
        if self.selected_file is not None:
            min_date, max_date = self.selected_file.get_data_times()
            return min_date.astype("datetime64[s]"), max_date.astype("datetime64[s]")
        else:
            return np.datetime64("NaT"), np.datetime64("NaT")

    def build_file_dict(self):
        return {k.stem: k for k in self.get_file_list()}

    def get_options(self):
        value_list = [str(k) for k in self.build_file_dict().keys()]
        value_list = sort(value_list)
        value_list = value_list[::-1]  # reverse list
        return value_list

    def update_loaded_time_span(self, hour_delay, min_delay):
        self.parameters.time_span_hour = hour_delay
        self.parameters.time_span_min = min_delay
        if self.selected_file is not None:
            self.selected_file.set_loaded_time_span(hour_delay * 3600 + min_delay * 60)

    def is_loading(self):
        if self.selected_file is not None:
            return self.selected_file.is_loading()
        return False

    def reload(self):
        if self.selected_file is not None:
            self.selected_file.read_file(block=True)
