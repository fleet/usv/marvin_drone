"""Store id used by callback and app"""
DASH_REFRESH = "refresh"


UPDATE_GRAPH_BUTTON = "btn-update-transducers-id"

GRAPH_CONTAINER = "hermes-tranducers-container-id"

GRAPH_MIN_MAX_COLOR = "hermess-min-max-color-value"


# DATE_TIME_LABEL = "hermers-date-time-label"
FILE_REFRESH = "btn-refresh-filelist"
FILE_DROP_DOWN_ID = "hermes-file-drop-down-id"
FILE_LABEL = "hermes-file-label-id"

DATE_SELECTION = "dpd-hour-numbers-selected"
HOUR_DELAY_INPUT = "hour-delay-input"
MIN_DELAY_INPUT = "min-delay-input"

AUTO_REFRESH_INPUT = "enable-auto-refresh-input"
HIDDEN_LABEL = "empty-label"

REDRAW_EVENT_LABEL = "redraw-label"
RELOAD_EVENT_LABEL = "reload-label"
RELOAD_LOADING_WIDGET = "load-loading-widget"

PERIODIC_REFRESH_EVENT = "periodic-refresh-event-label"
