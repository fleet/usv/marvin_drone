import time
from datetime import datetime

import dash
import dash_bootstrap_components as dbc
import numpy as np
import plotly.express as px
from dash import Dash, html, Output, Input, dcc
from dash.exceptions import PreventUpdate

from marvin.land.components.hermes import hermes_ids
from marvin.land.components.hermes.controler import HermesControler
from marvin.land.server.ek80.ek80_colormap import cmap_plotly


class RenderingParameters:
    min_color_value = -100
    max_color_value = 100

    min_possible_color_value = -100
    max_possible_color_value = 100




def render(app: Dash, data_controler: HermesControler) -> html.Div:
    parameters = RenderingParameters()

    def build_depth():
        values = data_controler.get_time_values()
        values_arr = values.depth.data * -1
        return px.line(x=values.time,y=values_arr,labels={'x':"ping time",'y':"bottom elevation"},height=250)


    def build_interping():
        values = data_controler.get_time_values()
        interping = np.diff(values.time)
        ping_number_diff = np.diff(values.ping_number)
        helper = np.vectorize(lambda x: x.total_seconds())
        dt_sec = helper(interping)
        ping_rate = dt_sec / ping_number_diff
        times=values.time[:-1:]

        return px.line(x=times,y=ping_rate,labels={'x':"ping time",'y':"ping rate (sec)"},height=250)

    def build_single_graphic(transducer):
        data = data_controler.get_data(transducer_name=transducer)
        data = data.transpose("indicative_range", "time")
        fig = px.imshow(data, title=transducer, zmax=parameters.max_color_value, zmin=parameters.min_color_value,
                        color_continuous_scale=cmap_plotly)
        return fig

    def build_graphics():
        """return an array of dash Graph"""
        graphics = []
        start = time.time()
        print(f"Start of build graphics")
        if data_controler.selected_file is None:
            return graphics
        g = dcc.Graph(figure=build_depth())
        graphics.append(g)
        g = dcc.Graph(figure=build_interping())
        graphics.append(g)

        for transducer in data_controler.get_transducer_list():
            g = dcc.Graph(figure=build_single_graphic(transducer))
            graphics.append(g)
        elapsed = time.time() - start
        print(f"End of build graphics in {elapsed}s")

        return graphics

    @app.callback(
        Output(hermes_ids.GRAPH_CONTAINER, "children"),
        Input(hermes_ids.GRAPH_MIN_MAX_COLOR, "value"),
        Input(hermes_ids.RELOAD_EVENT_LABEL,"children")

    )
    def on_redraw(value_treshold, reload_event):  # pylint: disable=unused-argument
        if value_treshold is not None:
            parameters.min_color_value = value_treshold[0]
            parameters.max_color_value = value_treshold[1]
        return get_graphics()

    @app.callback(
        Output(hermes_ids.PERIODIC_REFRESH_EVENT, "children"),
        Input(hermes_ids.DASH_REFRESH, "n_intervals")
    )
    def on_periodic_refresh(n): # pylint: disable=unused-argument
        "Ignore periodic refresh if not checked"
        if dash.callback_context.triggered_id == hermes_ids.DASH_REFRESH and not data_controler.parameters.enable_auto_refresh:
            raise PreventUpdate

        if data_controler.is_loading():
            raise PreventUpdate

        return f""


    @app.callback(
        Output(hermes_ids.RELOAD_EVENT_LABEL, "children"),
        Input(hermes_ids.UPDATE_GRAPH_BUTTON, "n_clicks"),
        Input(hermes_ids.PERIODIC_REFRESH_EVENT, "children"),
        Input(hermes_ids.FILE_LABEL, "children")
    )
    def on_reload(n_clicks,values,file_lab):  # pylint: disable=unused-argument
        if data_controler.is_loading():
            print(f" ignore event, already loading something")
            raise PreventUpdate

        print('start reload')
        data_controler.reload()
        print("done")

        if data_controler.selected_file is None:
            return "no selection"
        else:
            min_time, max_time = data_controler.get_data_times()
            return f"File : {data_controler.selected_file.file_name.stem}, data times = [{np.datetime_as_string(min_time, unit='s')} - {np.datetime_as_string(max_time, unit='s')}]"



    @app.callback(
        Output(hermes_ids.HIDDEN_LABEL, "children"),
        Input(hermes_ids.AUTO_REFRESH_INPUT, "value"),
        Input(hermes_ids.MIN_DELAY_INPUT, "value"),
        Input(hermes_ids.HOUR_DELAY_INPUT, "value")
    )
    def on_auto_refresh_changed(value,minute_delay, hour_delay):
        if len(value) == 1 and value[0]==1:
            data_controler.parameters.enable_auto_refresh = True
        else:
            data_controler.parameters.enable_auto_refresh = False

        if data_controler.parameters.time_span_min != minute_delay or data_controler.parameters.time_span_hour != hour_delay:
            #we need to change loading parameters
            data_controler.update_loaded_time_span(hour_delay, minute_delay)

        return f""

    def get_graphics():
        values = build_graphics()

        min_time, max_time = data_controler.get_data_times()
        time_now = np.datetime64(datetime.utcnow())  # {datetime64} 2018-09-17T14:48:16.599541
        diff = time_now - max_time
        diff_s = diff / np.timedelta64(1, 's')
        if diff_s > data_controler.parameters.datamodel_last_date_timeout:
            return dbc.Row(values,style = {"background-color": "red"} ) # we emit a warning if data is too old.
        return dbc.Row(values)

    hour_delay_input = dcc.Input(
        id=hermes_ids.HOUR_DELAY_INPUT,
        type="number",
        size=5,
        min=0,
        step=1,
        value=data_controler.parameters.time_span_hour
    )
    min_delay_input = dcc.Input(
        id=hermes_ids.MIN_DELAY_INPUT,
        type="number",
        size=5,
        min=0,
        max=60,
        step=1,
        value=data_controler.parameters.time_span_min
    )
    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("Hermes transducers data"),
                            dbc.Row(
                                [
                                    dbc.Col(
                                        dbc.Button(
                                            "Refresh", id=hermes_ids.UPDATE_GRAPH_BUTTON, n_clicks=0
                                        )),
                              dbc.Col(
                                        [
                                        dbc.Checklist(
                                            options=[
                                                {f"label": f"Auto refresh ({data_controler.parameters.auto_refresh_period}) in seconds", "value": 1},
                                            ],
                                            value=[1 if data_controler.parameters.enable_auto_refresh else 0],
                                            id=hermes_ids.AUTO_REFRESH_INPUT,
                                        ),
                                        html.Div(id=hermes_ids.HIDDEN_LABEL, children="_N_")
                                        ])
                                ]

                            ),
                            html.Hr(),

                            dbc.Col(
                                [
                                    "Display last ",
                                    hour_delay_input,
                                    " hour(s) ",
                                    min_delay_input,
                                    " minute(s)",

                                ]
                            )
                            ,

                            dbc.Col(
                                dbc.Row(
                                    [html.Div("Color min max values"),
                                     dcc.RangeSlider(parameters.min_possible_color_value,
                                                     parameters.max_possible_color_value,
                                                     value=[parameters.min_color_value, parameters.max_color_value],
                                                     id=hermes_ids.GRAPH_MIN_MAX_COLOR, tooltip=True)
                                     ]
                                )
                            ),
                            html.Hr(),
                            dcc.Loading(
                                id=hermes_ids.RELOAD_LOADING_WIDGET,
                                type="default",
                                children=html.Div(id=hermes_ids.RELOAD_EVENT_LABEL)
                            ),


                            # dcc.Input(type='time'),


                            html.Div(id=hermes_ids.GRAPH_CONTAINER, children=get_graphics()),
                            html.Div("", id="label", hidden=True),
                            html.Div("",id=hermes_ids.PERIODIC_REFRESH_EVENT,hidden=True)
                        ]
                    ),
                ]
            )
        ]
    )
