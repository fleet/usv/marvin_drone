import dash_bootstrap_components as dbc
from dash import Dash, html, Output, Input, ctx, dcc

from marvin.land.components.hermes import hermes_ids
from marvin.land.components.hermes.controler import HermesControler


def render(app: Dash, data_controler: HermesControler) -> html.Div:
    # call back


    dropdown = dcc.Dropdown(
        options = data_controler.get_options(),
        placeholder="Select a file",
        id=hermes_ids.FILE_DROP_DOWN_ID,
    )

    @app.callback(
        Output(hermes_ids.FILE_LABEL,"children"),
        Input(hermes_ids.FILE_DROP_DOWN_ID, "value")
    )
    def on_selection(value):  # pylint: disable=unused-argument
        #retrieve file_path
        if value is None:
            return "No file selected"
        file_dict = data_controler.build_file_dict()
        data_controler.set_selected_file(file_dict[value])

        return f"file {value} selected"

    @app.callback(
            Output(hermes_ids.FILE_DROP_DOWN_ID, "options"), Input(hermes_ids.FILE_REFRESH, "n_clicks")
    )
    def periodic_refresh_agent_list(n):  # pylint: disable=unused-argument
        return data_controler.get_options()


    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("Hermes file list"),
                            dbc.Row(
                                [
                                    dbc.Col(dropdown, width=4),
                                    dbc.Col(
                                        dbc.Button(
                                            "Refresh file list", id=hermes_ids.FILE_REFRESH, n_clicks=0
                                        ),
                                    ),
                                ]
                            ),
                            html.Div("No file selected",id=hermes_ids.FILE_LABEL)
                        ]
                    ),
                ]
            )
        ]
    )
