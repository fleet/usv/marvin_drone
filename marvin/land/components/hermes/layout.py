from dash import Dash, html, dcc, Output
import dash_bootstrap_components as dbc

from marvin.land.model.data_model import DataControler
from . import hermes_ids, file_choice, data_display

from .controler import HermesControler







def create_layout(app: Dash, controler: HermesControler) -> html.Div:

    component = html.Div(
        [
            html.Div(
                className="app-div",
                children=dbc.Container(
                    [
                        file_choice.render(app=app, data_controler=controler),
                        data_display.render(app=app, data_controler=controler),
                    ]
                ),
            ),
            dcc.Interval(
                id=hermes_ids.DASH_REFRESH,
                interval=controler.parameters.auto_refresh_period * 1000,  # in milliseconds
                n_intervals=0,
            ),
        ]
    )

    return component
