import dash_bootstrap_components as dbc
from dash import Dash, html, Output, Input, ctx, dcc
from dash.exceptions import PreventUpdate

from marvin.common.agent.agent import AgentIds
from marvin.land.components import ids
from marvin.land.model.data_model import (
    DataControler,
    AgentStatusModel,
)



def render(app: Dash, data_controler: DataControler) -> html.Div:
    """Widget for start/stop command of"""

    def has_adcp():
        agents = data_controler.datamodel.agent_status.as_dict()
        names = agents[AgentStatusModel.AGENT_COL]
        return AgentIds.adcp_drone in names


    def get_config_values():
        """Retrieve the list of avalaible configuration for adcp rdi agents"""
        return data_controler.adcp.get_config_list()

    dropdown = dcc.Dropdown(
        get_config_values(),
        id=ids.ADCP_CONFIG_DROPDOWN
    )

    last_messages =[""]

    @app.callback(
        Output(ids.ADCP_CONFIG_DROPDOWN, "options"), Input(ids.DASH_REFRESH, "n_intervals")
    )
    def periodic_refresh_agent_list(n):  # pylint: disable=unused-argument
        return get_config_values()

    @app.callback(
        Output(ids.ADCP_SEND_CONFIG_LOG, "children"),
        Input(ids.ADCP_SEND_CONFIG_BTN, "n_clicks"),
        Input(ids.ADCP_CONFIG_DROPDOWN, "value"),
    )
    def displayClick(
        n_clicks, selected_config
    ):  # pylint: disable=unused-argument
        if ids.ADCP_CONFIG_DROPDOWN == ctx.triggered_id:
            #raise PreventUpdate it seems that periodic refresh prevent to update messages because sending configuration is probably too slow
            return html.Div(last_messages[0])

        # find selected agent
        filtered_config = [conf for conf in get_config_values() if conf == selected_config]
        if len(filtered_config) == 0:
            last_messages[0]="No configuration selected"
        else:
            try:
                if ids.ADCP_SEND_CONFIG_BTN == ctx.triggered_id:
                    last_messages[0] = f"Sending request for {selected_config} configuration "
                    data_controler.adcp.send_config(config=selected_config)
            except Exception as e:
                last_messages[0] = f"An exception occured : {e}"
        return html.Div(last_messages[0])


    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("ADCP config command"),
                            dbc.Row(
                                [
                                    dbc.Col(dropdown, width=4),
                                    dbc.Col(
                                        [
                                            dbc.Button(
                                                "Send", id=ids.ADCP_SEND_CONFIG_BTN, n_clicks=0
                                            ),
                                        ],
                                        width=2,
                                    ),
                                    dbc.Col(html.Div(id=ids.ADCP_SEND_CONFIG_LOG)),
                                ]
                            ),
                        ]
                    ),
                ]
            )
        ]
    )
