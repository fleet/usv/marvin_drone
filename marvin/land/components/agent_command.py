import dash_bootstrap_components as dbc
from dash import Dash, html, Output, Input, ctx, dcc

from marvin.land.components import ids
from marvin.land.model.data_model import (
    DataControler,
    AgentStatusModel,
)


def render(app: Dash, data_controler: DataControler) -> html.Div:
    """Widget for start/stop command of"""

    def retrieve_agent_list():
        agents = data_controler.datamodel.agent_status.as_dict()
        names = agents[AgentStatusModel.AGENT_COL]
        ids_list = agents[AgentStatusModel.AGENT_ID]
        agent_dict =  dict(zip(ids_list, names))
        return agent_dict

    def get_agent_values():
        return [str(v) for k, v in retrieve_agent_list().items()]

    dropdown = dcc.Dropdown(
        get_agent_values(),
        id=ids.AGENT_DROP_DOWN_ID,
    )

    @app.callback(
        Output(ids.AGENT_DROP_DOWN_ID, "options"), Input(ids.DASH_REFRESH, "n_intervals")
    )
    def periodic_refresh_agent_list(n):  # pylint: disable=unused-argument
        return get_agent_values()

    @app.callback(
        Output("out-component", "children"),
        Input(ids.START_AGENT, "n_clicks"),
        Input(ids.STOP_AGENT, "n_clicks"),
        Input(ids.AGENT_DROP_DOWN_ID, "value"),
    )
    def displayClick(
        start_btn, stop_btn, selected_agent
    ):  # pylint: disable=unused-argument
        msg = ""
        # find selected agent
        agent_id = None
        for k, v in retrieve_agent_list().items():
            if v == selected_agent:
                agent_id = k
        if agent_id is None:
            return html.Div("No agent selected")
        try:
            if ids.STOP_AGENT == ctx.triggered_id:
                msg = f"Request stop for {selected_agent}"
                data_controler.send_stop_agent(agent_id=agent_id)
            elif ids.START_AGENT == ctx.triggered_id:
                msg = f"Request start for {selected_agent}"
                data_controler.send_start_agent(agent_id=agent_id)
        except Exception as e:
            msg = f"An exception occured : {e}"
        return html.Div(msg)


    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H2("Agent command"),
                            dbc.Row(
                                [
                                    dbc.Col(dropdown, width=4),
                                    dbc.Col(
                                        [
                                            dbc.Button(
                                                "Start", id=ids.START_AGENT, n_clicks=0
                                            ),
                                            dbc.Button(
                                                "Stop", id=ids.STOP_AGENT, n_clicks=0
                                            ),
                                        ],
                                        width=2,
                                    ),
                                    dbc.Col(html.Div(id="out-component")),
                                ]
                            ),
                        ]
                    ),
                ]
            )
        ]
    )
