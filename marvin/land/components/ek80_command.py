import dash_bootstrap_components as dbc
from dash import Dash, html, Output, Input, ctx, dcc

from marvin.land.components import ids
from marvin.land.model.data_model import (
    DataControler,
    AgentStatusModel,
)


def render(app: Dash, data_controler: DataControler) -> html.Div:
    """Widget for start/stop command of EK80 pinging"""

    def start_direct_ping():
        data_controler.ek80.direct_start()

    def stop_direct_ping():
        data_controler.ek80.direct_stop()

    def switch_to_cw_mode():
        data_controler.ek80.direct_cw()


    @app.callback(
        Output(ids.EK80_MESSAGE, "children"),
        Input(ids.START_EK80_PING, "n_clicks"),
        Input(ids.STOP_EK80_PING, "n_clicks"),
        Input(ids.EK80_TO_CW, "n_clicks")
    )
    def displayClick(
        start_btn, stop_btn, cw_btn
    ):  # pylint: disable=unused-argument
        msg = ""
        # find selected agent
        agent_id = None

        try:
            if ids.START_EK80_PING == ctx.triggered_id:
                msg = f"Request to switch to normal mode and start pinging"
                start_direct_ping()
            elif ids.STOP_EK80_PING == ctx.triggered_id:
                msg = f"Request stop pinging"
                stop_direct_ping()
            elif ids.EK80_TO_CW == ctx.triggered_id:
                msg = f"Request stop pinging"
                switch_to_cw_mode()
        except Exception as e:
            msg = f"An exception occured : {e}"

        return html.Div(msg)

    return html.Div(
        children=[
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            dbc.Row([
                                dbc.Col([
                                    html.H2("EK80 specific commands"),

                                    html.H4("Bypass Hermes and send command directly to EK80"),

                                    html.Div(
                                        "Two commands are available : [switch EK80 software to normal mode and start pinging] and [stop pinging]"),

                                ]),
                                dbc.Col(
                                    dbc.Row([
                                        dbc.Button(
                                            "Start", id=ids.START_EK80_PING, n_clicks=0, outline=True, color="primary"
                                        ),
                                        dbc.Button(
                                            "Stop", id=ids.STOP_EK80_PING, n_clicks=0, outline=True, color="primary"
                                        ),
                                        dbc.Button(
                                            "Switch to CW", id=ids.EK80_TO_CW, n_clicks=0, outline=True, color="primary"
                                        ),
                                    ]),

                                )
                            ]),


                            dbc.Col(html.Div(id=ids.EK80_MESSAGE)),

                        ]
                    ),
                ]
            )
        ]
    )
