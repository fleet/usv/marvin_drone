from __future__ import annotations
import datetime
import time
import traceback
from collections import OrderedDict
from enum import IntEnum, auto
from threading import Thread, Lock
from typing import Dict, Any, List, Set
import json

import pandas as pd
from pydantic import BaseModel
import requests
from requests import HTTPError

from marvin.common import api_path
from marvin.common.agent import supervisor
from marvin.common.api_path import AgentStatusList as ApiStatusList, compute_agent_api, rdi_config_id, ADCPRDIConfig, \
    rdi_config_fullpath, rdi_load_config_fullpath, ConfigParameter
from marvin.common.common_parameters import ek80_port
from marvin.common.data import DataStatusEnum
from marvin.land import parameters
from marvin.land.parameters import LandParameters
from marvin.land.utils.Log import Logger
from marvin.land.utils.ThreadLoop import ThreadLoop
import marvin.common.common_parameters as supervisor_param

API_TIME_OUT = 30


class EnumConnectionStatus(IntEnum):
    UNKNOWN = auto()
    ERROR = auto()
    SUCCESS = auto()


class ConnectionStatus:
    """Connexion status class, retain datetime when value changed"""

    def __init__(
        self,
        supervisor: SupervisorDesc | None = None,
        status=EnumConnectionStatus.UNKNOWN,
    ):
        self._status = status
        self._time = datetime.datetime.utcnow()
        self._supervisor = supervisor

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        if value != self._status:
            self._time = datetime.datetime.utcnow()
        self._status = value

    @property
    def update_time(self):
        return self._time


class SupervisorDesc(BaseModel):
    name: str
    address: str
    port: int
    agent_names: Set[str] = set()


class SupervisorModel(BaseModel):
        supervisors: List[SupervisorDesc]


class AgentStatusModel:
    """
    Controler or Datamodel giving access to the agent_status of all agents
    """

    AGENT_COL = "Agents"
    STATUS_COL = "Status"
    TIME_COL = "Status update Time"
    DATA_COL = "Data production status"
    LAST_FILE = "Last file"
    LAST_FILE_TIME = "Last file update"

    AGENT_ID = "Agent id"

    def __init__(self, value: ApiStatusList = ApiStatusList(agents={})):
        self.data = self.__from_api(value=value)

    @staticmethod
    def __from_api(value: ApiStatusList) -> Dict[str, List[Any]]:
        """
        compute datamodel from ApiStatusList
        """
        names = [v.name for k, v in value.agents.items()]
        status = [v.status for k, v in value.agents.items()]
        dates = [v.status_time for k, v in value.agents.items()]

        data_status_raw = [v.data_status for k, v in value.agents.items()]
        data_status = [
            s.status if s is not None else DataStatusEnum.UNKNOWN
            for s in data_status_raw
        ]

        last_file = [s.file_path if s is not None else None for s in data_status_raw]
        last_update = [
            s.last_modified_date if s is not None else None for s in data_status_raw
        ]

        agents_id = value.agents.keys()

        data = OrderedDict(
            [
                (AgentStatusModel.AGENT_COL, names),
                (AgentStatusModel.STATUS_COL, status),
                (AgentStatusModel.TIME_COL, dates),
                (AgentStatusModel.AGENT_ID, agents_id),
                (AgentStatusModel.LAST_FILE, last_file),
                (AgentStatusModel.LAST_FILE_TIME, last_update),
                (AgentStatusModel.DATA_COL, data_status),
            ]
        )

        return data

    def as_dict(self):
        return self.data

    def as_pandas(self):
        """Retrieve all agent agent_status as a panda dataframe"""
        return pd.DataFrame(self.data)


class DataModel:
    def __init__(
        self,
        supervisors_model: SupervisorModel,
        agent_status: AgentStatusModel,
        connexion_status: ConnectionStatus,
    ):
        self.supervisors_model = supervisors_model
        self.agent_status = agent_status
        self.connexion_status = connexion_status

class ADCPControler:
    def __init__(self, parameters: LandParameters):
        self.parameters = parameters

    def send_config(self, config:str):

        request_url = (
            f"http://{self.parameters.drix_ip_adress}:{self.parameters.drix_api_port}/{rdi_load_config_fullpath}"
        )
        headers = {"Content-type": "application/json", "Accept": "application/json"}

        data= ConfigParameter(configuration_name=config)
        data.json()
        response = requests.put(
            request_url,
            data=data.json(),
            headers=headers,
            timeout=self.parameters.requests_timeout,
        )

        response.raise_for_status()  # raise an exception if an http error occured

    def get_config_list(self):
        """return the list of avalaible configs for adcp"""
        try:
            # retrieve the list of agents and status

            request_url = f"http://{self.parameters.drix_ip_adress}:{self.parameters.drix_api_port}/{rdi_config_fullpath}"
            response = requests.get(request_url, timeout=API_TIME_OUT)

            response.raise_for_status()  # raise an exception if an http error occured
            result = ADCPRDIConfig.parse_obj(response.json())
            return [str(v) for v in result.config_files.keys()]
        except HTTPError as e:
            Logger.warning(f"Http connexion error {e}")
        except Exception as g:
            Logger.warning(f"Unknown error while retrieving status {g}")

        return []


class EK80Controler:
    def __init__(self, parameters: LandParameters):
        self.parameters = parameters

    def _switch_to_normal_mode(self):
        # switch to normal mode
        request_url = f"http://{self.parameters.drix_ip_adress}:{ek80_port}/api/system/operation-mode"
        headers = {"Content-type": "application/json", "Accept": "application/json"}
        response = requests.put(
            request_url,
            data="'normal'",
            headers=headers,
            timeout=self.parameters.requests_timeout,
        )
        response.raise_for_status()  # raise an exception if an http error occured

    def _set_play_state(self, state="'running'"):
        "running"

        request_url = (
            f"http://{self.parameters.drix_ip_adress}:{ek80_port}/api/system/play-state"
        )
        headers = {"Content-type": "application/json", "Accept": "application/json"}
        response = requests.put(
            request_url,
            data=state,
            headers=headers,
            timeout=self.parameters.requests_timeout,
        )
        response.raise_for_status()  # raise an exception if an http error occured

    def direct_start(self):
        try:
            self._switch_to_normal_mode()
            self._set_play_state(state="'running'")
        except HTTPError as e:
            Logger.warning(f"Http connexion error {e}")
            raise e

    def direct_cw(self):
        try:
            self._switch_to_cw_mode()
        except HTTPError as e:
            Logger.warning(f"Http connexion error {e}")
            raise e

    def direct_stop(self):
        "stopped"
        try:
            self._switch_to_normal_mode()
            self._set_play_state(state="'stopped'")
        except HTTPError as e:
            Logger.warning(f"Http connexion error {e}")
            raise e

    def _switch_to_cw_mode(self):
        # switch to normal mode
        parameters_70 = '{\
  "pulse-type": "cw",\
  "pulse-duration": 0.001024,\
  "start-frequency": 70000,\
  "end-frequency": 70000,\
  "channel-mode": "active",\
  "ramping": "fast",\
  "filter-type": "standard-resolution"\
}'

        parameters_200 = '{\
  "pulse-type": "cw",\
  "pulse-duration": 0.001024,\
  "start-frequency": 200000,\
  "end-frequency": 200000,\
  "channel-mode": "active",\
  "ramping": "fast",\
  "filter-type": "standard-resolution"\
}'
        request_url = f"http://{self.parameters.drix_ip_adress}:{ek80_port}/api/sounder/ping-configuration/channel-list"
        headers = {"Content-type": "application/json", "Accept": "application/json"}
        response = requests.get(request_url, timeout=API_TIME_OUT)
        response.raise_for_status()

        transducer_list = json.loads(response.text)
        # TODO retrieve transducers from real list
        def get_parameter(name:str):
            if "ES200" in name:
                return parameters_200
            if "ES70" in name:
                return parameters_70
            raise NotImplementedError("Transducer frequency not implemented")

        for transducer in transducer_list:

            request_url = f"http://{self.parameters.drix_ip_adress}:{ek80_port}/api/sounder/ping-configuration/{transducer}/pulse-settings"
            headers = {"Content-type": "application/json", "Accept": "application/json"}
            parameters = get_parameter(transducer)
            response = requests.put(
                request_url,
                data=parameters,
                headers=headers,
                timeout=self.parameters.requests_timeout,
            )
            Logger.info(f"PUT {request_url} value {parameters}")
            response.raise_for_status()  # raise an exception if an http error occured



class DataControler(ThreadLoop):
    def __init__(self):
        self.parameter = parameters.LandParameters()
        super().__init__(refresh_period=self.parameter.datamodel_refresh_period)

        drone_supervisor = SupervisorDesc(
            name="Drone agents supervisor",
            address=supervisor_param.drone_host,
            port=supervisor_param.drone_port,
        )
        land_supervisor = SupervisorDesc(
            name="Land agents supervisor",
            address=supervisor_param.land_host,
            port=supervisor_param.land_port,
        )

        self.datamodel = DataModel(
            supervisors_model=SupervisorModel(
                supervisors=[drone_supervisor, land_supervisor]
            ),
            agent_status=AgentStatusModel(),
            connexion_status=ConnectionStatus(status=EnumConnectionStatus.UNKNOWN),
        )
        self.ek80 = EK80Controler(self.parameter)
        self.adcp = ADCPControler(self.parameter)

    def _initialize(self):
        pass

    def _periodic_process(self):
        self.refresh_status()

    class AgentCommand(IntEnum):
        START = auto()
        STOP = auto()

    def _send_agent_cmd(self, agent_id: str, command: AgentCommand):
        """Send a command to agent"""

        agent_supervisor = None
        for supervisor in self.datamodel.supervisors_model.supervisors:
            if agent_id in supervisor.agent_names:
                agent_supervisor = supervisor

        if agent_supervisor is None:
            Logger.warning(
                f"Launch command failed : supervisor not found for agent {agent_id}"
            )
            return

        # retrieve the list of agents and status
        route = api_path.supervisor_agent_start
        if command == DataControler.AgentCommand.STOP:
            route = api_path.supervisor_agent_stop

        request_url = f"{self.__get_supervisor_url(agent_supervisor)}{compute_agent_api(route=route, agent_id=agent_id)}"
        response = requests.put(request_url, timeout=API_TIME_OUT)
        response.raise_for_status()  # raise an exception if an http error occured

    def send_start_agent(self, agent_id: str):
        """query to start agent"""
        self._send_agent_cmd(agent_id=agent_id, command=self.AgentCommand.START)

    def send_stop_agent(self, agent_id: str):
        """query to start agent"""
        self._send_agent_cmd(agent_id=agent_id, command=self.AgentCommand.STOP)

    def refresh_status(self):
        # Keep supervisors list
        supervisors_model = self.datamodel.supervisors_model

        # Merge all agent status in a single dict
        connection_status = ConnectionStatus(status=EnumConnectionStatus.SUCCESS)
        agent_status = {}
        for supervisor in supervisors_model.supervisors:
            agents_status_of_supervisor = self.__request_agents_status(supervisor)
            if agents_status_of_supervisor is not None:
                agent_status = agent_status | agents_status_of_supervisor.agents
            else:
                connection_status = ConnectionStatus(
                    supervisor=supervisor, status=EnumConnectionStatus.ERROR
                )
        self.datamodel = DataModel(
            supervisors_model,
            AgentStatusModel(ApiStatusList(agents=agent_status)),
            connection_status,
        )

    def __request_agents_status(
        self, supervisor: SupervisorDesc
    ) -> ApiStatusList | None:
        try:
            # retrieve the list of agents and status
            #Logger.info(f"Requesting agent status from {supervisor.name}")
            request_url = f"{self.__get_supervisor_url(supervisor)}{api_path.supervisor_agent_status}"
            #Logger.info(f"Requesting agent status from {request_url}")

            response = requests.get(request_url, timeout=API_TIME_OUT)

            response.raise_for_status()  # raise an exception if an http error occured
            result = ApiStatusList.parse_obj(response.json())
            if len(supervisor.agent_names) == 0:
                supervisor.agent_names = result.agents.keys()
            return result
        except HTTPError as e:
            Logger.warning(f"Http connexion error {e}")
        except Exception as g:
            Logger.warning(f"Unknown error while retrieving status {g}")
        return None

    def __get_supervisor_url(self, supervisor: SupervisorDesc) -> str:
        return f"http://{supervisor.address}:{supervisor.port}"



