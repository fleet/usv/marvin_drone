from pydantic import BaseModel


class DataRetrieveParameters(BaseModel):
    drone_directory :str = None
    land_directory :str= None
    data_pattern = []
    file_monitor_period = 10
    use_rsync = False
    datastatus_timeout = (60*1)
