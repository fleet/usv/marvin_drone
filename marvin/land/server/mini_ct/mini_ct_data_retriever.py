""""
Manage ADCP agent
"""
import datetime
import time
from multiprocessing import Process
from pathlib import Path
from typing import List

import pytz

from marvin.common.agent.agent import Agent
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatus, DataStatusEnum
from marvin.common.file_watcher import DataStatusMonitor, FileEvent
from marvin.drone.system import common_logger
from marvin.drone.system.services.services import Services
from marvin.land.server.adcp.adcp_file_processing import run_as_process
from marvin.land.server.adcp.adcp_parameters import AdcpAgentParameters
from marvin.common.processes import _kill_processes as kill_processes
from marvin.land.server.data_retriever_parameter import DataRetrieveParameters
from marvin.land.server.local_agent_data_retriever import DataRetriever


class CTParameters(DataRetrieveParameters):
    drone_directory = r"\\192.168.1.100\e\QINSY PROJECT\822C36_DRIX_IFREMER_CSWOT\LogFiles\MiniCT_Logs\Hourly"
    land_directory = r"E:\CSWOT\MiniCT\Hourly"
    data_pattern = ["*.txt"]
    use_rsync = False
    datastatus_timeout = (60*2) #file is changed every hour, check every minute
    file_monitor_period = 60 #monitor every minute


class MiniCTDataRetriever(DataRetriever):
    """
    Land agent, inspecting a shared directory on the drone and retrieving file changes on land
    """

    def __init__(self):
        super().__init__(name="Mini CT data retrieval Agent", agent_id="MiniCT",parameters=CTParameters())

