import time
from pathlib import Path
from threading import Thread
from typing import List

from marvin.land import parameters
from marvin.land.server.context import Context

from marvin.land.server.agent_api import ApiParser
from marvin.land.server.hydrophone.hydrophone_processor import HydroProcessor, AgentProcessor
from marvin.land.utils.ThreadLoop import ThreadLoop


class DataServer(ThreadLoop):
    def __init__(self, working_directory: Path):
        self.parameter = parameters.LandParameters()
        self.context = Context(working_dir=working_directory)
        self.logger = self.context.logger
        super().__init__(refresh_period=self.parameter.datamodel_refresh_period)
        self.data_processor_list: List[AgentProcessor] = []

    def _initialize(self):
        """
        initialize server dataprocessing
        """
        # establish a first connection to drix and retrieve agent list, which is supposed here to be immutable
        parser = ApiParser(logger=self.context.logger, parameters=self.parameter)
        agent_list = None
        while agent_list is None:
            agent_list = parser.get_agent_list()
            if agent_list is None:
                self.logger.info("Wait")
                time.sleep(self.parameter.datamodel_refresh_period)
        self.logger.info(f"Agent list retrieved : {agent_list}")

        # parse agent list and create agent data processor, these are in charge of retrieving data samples and archive these to database
        for agent in agent_list.agents:
            if agent == "hydrophone":
                self.data_processor_list.append(HydroProcessor(context=self.context))

        for data_processor in self.data_processor_list:
            data_processor.on_start()

    def _periodic_process(self):
        """
        Do real periodic processing

        """
        self.context.logger.debug(f"Periodic processing {__file__}:_periodic_process")
        for proc in self.data_processor_list:
            proc.periodic_process()
