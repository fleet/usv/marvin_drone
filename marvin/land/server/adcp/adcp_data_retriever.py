""""
Manage ADCP agent
"""
import datetime
import time
from multiprocessing import Process
from pathlib import Path
from typing import List

import pytz

from marvin.common.agent.agent import Agent
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatus, DataStatusEnum
from marvin.common.file_watcher import DataStatusMonitor, FileEvent
from marvin.drone.system import common_logger
from marvin.drone.system.services.services import Services
from marvin.land.server.adcp.adcp_file_processing import run_as_process
from marvin.land.server.adcp.adcp_parameters import AdcpAgentParameters
from marvin.common.processes import _kill_processes as kill_processes
from marvin.land.server.local_agent_data_retriever import DataRetriever



class AdcpDataRetriever(DataRetriever):
    """
    Land agent, inspecting a shared directory on the drone and retrieving file changes on land
    """

    def __init__(self):
        super().__init__(name="ADCP data retrieval Agent", agent_id="ADCP",parameters=AdcpAgentParameters())

