from marvin.land.server.data_retriever_parameter import DataRetrieveParameters


class AdcpAgentParameters(DataRetrieveParameters):
    drone_directory = r"\\192.168.1.100\e\ADCP_DATA"
    land_directory = r"E:\CSWOT\ADCP"
    data_pattern = ["*.STA"]
    use_rsync = True
