
"""
Handle in a separate process the copy of ADCP files from drone to land
"""
import functools
import os
import shutil
from multiprocessing import Process
from os import PathLike
from pathlib import Path
from typing import List

import marvin.land.utils.pyrsync2 as pyrsync
from marvin.common.file_watcher import FileEvent
from marvin.drone.system import common_logger
from marvin.land.server.data_retriever_parameter import DataRetrieveParameters


def __run(parameters: DataRetrieveParameters, file_events: List[FileEvent]):
    instance = CopyFileProcessor(
        parameters, [file_event.path for file_event in file_events]
    )
    instance.run()


def run_as_process(
    parameters: DataRetrieveParameters, file_events: List[FileEvent]
) -> Process:
    """Creates a CopyFileProcessor and execute run method on it"""
    return Process(target=functools.partial(__run, parameters, file_events))


class CopyFileProcessor:
    def __init__(
        self, parameters: DataRetrieveParameters, files_to_transfert: List[PathLike]
    ):
        self.parameters = parameters
        self.files_to_transfert = files_to_transfert
        self.logger = common_logger.configure(
            prefix="adcp_file_transfer", logger_name="adcp_file_transfer"
        )

    def run(self):
        """
        Execute data processing
        """
        for file_to_transfert in self.files_to_transfert:
            destination = Path(self.parameters.land_directory, file_to_transfert.name)
            if self.parameters.use_rsync and destination.exists():
                self.logger.info(f"Synchronizing {file_to_transfert} and {destination}")
                self._rsync(file_to_transfert, destination)
            else:
                self.logger.info(f"Copying {file_to_transfert} to {destination}")
                shutil.copyfile(file_to_transfert, destination)

    def _rsync(self, file_to_transfert: PathLike, destination: PathLike):
        """
        Synchronize the drone file with the land one using pyrsync2 library
        """
        sync_destination = destination.with_suffix(".sync")

        try:
            with open(destination, "rb") as land_file, open(
                file_to_transfert, "rb"
            ) as drone_file, open(sync_destination, "wb") as sync_land_file:
                # On the land file, compute checksums of blocks
                hashes = pyrsync.blockchecksums(land_file)

                # Compute the difference with the drone file
                delta = pyrsync.rsyncdelta(drone_file, hashes)

                # Apply the patch to the .sync file
                land_file.seek(0)
                pyrsync.patchstream(land_file, sync_land_file, delta)

            # The land file is replace by the .sync file
            os.remove(destination)
            os.rename(sync_destination, destination)

        except Exception as e:
            self.logger.error(
                f"An exception occured while synchronizing files {file_to_transfert} and {destination}  : {e}"
            )

        # Clean .sync file, whatever happens
        if sync_destination.exists():
            os.remove(sync_destination)
