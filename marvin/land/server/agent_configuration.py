from typing import Dict

from marvin.common.agent.agent import Agent
from marvin.land.server.adcp.adcp_data_retriever import AdcpDataRetriever
from marvin.land.server.mini_ct.mini_ct_data_retriever import MiniCTDataRetriever


def create_land_agent_list() -> Dict[str, Agent]:
    adcp = AdcpDataRetriever()
    ct = MiniCTDataRetriever()

    return {adcp.agent_id: adcp,ct.agent_id:ct}
