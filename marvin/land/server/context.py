"""
Python class for shared context and informations

"""
import logging
from pathlib import Path

from marvin.common import common_logger

class Context:
    def __init__(self,working_dir:Path):
        #init logging
        self.workding_dir = working_dir
        log_dir = working_dir / 'land' / "logs"
        self.logger = common_logger.configure(prefix= "data_server",log_directory=log_dir,logger_name="server_logger", level=logging.DEBUG)
