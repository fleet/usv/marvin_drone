"""Process hydrophone data samples"""
import abc
import functools
import time
from threading import Thread
from typing import Protocol, List

import pika

from marvin.common.hydrophone_api import MessageBroker, MessageEncoder
from marvin.land.server.context import Context


class AgentProcessor:
    def __init__(self, context: Context):
        self.context = context

    @abc.abstractmethod
    def periodic_process(self):
        """Do periodic processing"""

    @abc.abstractmethod
    def on_start(self):
        """Start processing"""


class MessageDecoder(Protocol):
    """A message decoder for hydrophone"""

    def decode(self, message):
        ...


class HydroProcessor(AgentProcessor):
    HYDROPHONE_DATA_SUBDIR = "hydrophone"

    def __init__(self, context: Context):
        super().__init__(context)
        self.data_directory = (
            context.workding_dir / "land" / HydroProcessor.HYDROPHONE_DATA_SUBDIR
        )
        # start to consume messages as a thread
        self.thread = Thread(
            target=self.message_broker_consumer
        )  # thread for status update control

    def on_start(self):
        self.start()

    def start(self):
        self._should_stop = False
        self.thread.start()

    def message_broker_consumer(self):
        message_broker_consumer = None
        try:
            while True:
                try:
                    message_broker_consumer = HydroMessageConsumer(self)
                    message_broker_consumer.consume()
                except pika.exceptions.AMQPConnectionError as e:
                    # In case of connection error we try to connect again
                    sleep_time = 1
                    self.context.logger.warning(
                        f"Connexion error to message broker, try to reconnect in {sleep_time}s"
                    )
                    time.sleep(sleep_time)
        except Exception as e:  # other exception like KeyboardInterrupt
            self.context.logger.warning(f"Exception {e} raised")
            if message_broker_consumer is not None:
                message_broker_consumer.close()

    def periodic_process(self):
        pass

    def decode(self, message):
        """
        decode message as a list of byte
        for hydrophone messages are xarray netcdf byte byffers
        """
        self.context.logger.debug(
            f"hydrophone message queue : message received size={len(message)}"
        )
        # deserialize message
        try:
            file_stem, dataset = MessageEncoder.decode(buffer=message)
            file_path = self.data_directory / (file_stem + ".nc")
            # save it to netcfd file
            dataset.to_netcdf(file_path)
        except Exception as e:
            self.context.logger.exception(
                f"Error while decoding hydrophone message : {e}"
            )


class HydroMessageConsumer:
    """
    Handle data message subscription and consumption

    """

    def __init__(self, decoder=MessageDecoder):
        self.decoder = decoder
        self.__create_connection()
        self.__setup()

    def __create_connection(self):
        credentials = pika.credentials.PlainCredentials(
            username="marvin", password="marvin"
        )
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=MessageBroker.broker_host, credentials=credentials
            )
        )

    def consume(self):
        self.channel.start_consuming()  # blocking function, will block until loop is finished

    def close(self):
        if self.channel is not None:
            self.channel.stop_consuming()
        if self.connection is not None:
            self.connection.close()

    def __setup(self):

        channel = self.connection.channel()
        self.channel = channel
        channel.exchange_declare(
            exchange=MessageBroker.exchange_name,
            exchange_type=MessageBroker.exchange_type,
        )

        result = channel.queue_declare(queue=MessageBroker.queue_name, exclusive=False)
        queue_name = result.method.queue

        channel.queue_bind(exchange=MessageBroker.exchange_name, queue=queue_name)

        # Callback wrapper to HydroMessageConsumer class
        def callback(ch, method, properties, body): #pylint: disable=unused-argument
            self.decoder.decode(message=body)

        channel.basic_consume(
            queue=queue_name, on_message_callback=callback, auto_ack=True
        )
