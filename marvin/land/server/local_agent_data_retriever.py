""""
Manage ADCP agent
"""
import datetime
import time
from multiprocessing import Process
from pathlib import Path
from typing import List

import pytz
from pydantic import BaseModel

from marvin.common.agent.agent import Agent
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatus, DataStatusEnum
from marvin.common.file_watcher import DataStatusMonitor, FileEvent
from marvin.drone.system import common_logger
from marvin.drone.system.services.services import Services
from marvin.land.server.adcp.adcp_file_processing import run_as_process
from marvin.common.processes import _kill_processes as kill_processes
from marvin.land.server.data_retriever_parameter import DataRetrieveParameters


class DataRetriever(Agent):
    """
    Land agent, inspecting a shared directory on the drone and retrieving file changes on land
    """



    def __init__(self, name, agent_id, parameters:DataRetrieveParameters):
        super().__init__(name=f"{name} data retrieval Agent", agent_id=agent_id)
        self.process_file_copy: Process = None
        self.current_parameters = parameters
        self.logger = common_logger.configure(
            prefix=f"{self.name}_agent", logger_name=f"{self.name.upper()}_LOGGER"
        )
        # Drone directory watcher
        self.status = AgentStatus.STOPPED
        self.data_status = DataStatus(
            status=DataStatusEnum.UNKNOWN,
            last_modified_date=datetime.datetime.fromtimestamp(0),
        )
        self.data_monitor: DataStatusMonitor = None

    def start(self):
        self.logger.info(f"Starting {self.name}...")
        self.status = AgentStatus.STARTING

        drone_directory = Path(self.current_parameters.drone_directory)
        if not drone_directory.exists():
            self.logger.error(
                f"{self.name} starting failed. Drone directory does not exist : {self.current_parameters.drone_directory}"
            )
            self.status = AgentStatus.FAILURE
            return

        land_directory = Path(self.current_parameters.land_directory)
        if not land_directory.exists():
            self.logger.error(
                f"{self.name} starting failed. Land directory does not exist : {self.current_parameters.land_directory}"
            )
            self.status = AgentStatus.FAILURE
            return

        # Drone directory watcher
        self.logger.info(
            f"Watching directory : {self.current_parameters.drone_directory}"
        )
        self.logger.info(f"Target directory : {self.current_parameters.land_directory}")
        self.data_monitor = DataStatusMonitor(
            directories=[drone_directory],
            filter_pattern=self.current_parameters.data_pattern,
            period_s=self.current_parameters.file_monitor_period,
        )

    def stop(self):
        self.logger.info(f"Stopping {self.name}...")
        if self.__is_data_monitor_running():
            self.data_monitor.process_datamonitor.kill()
        self.data_monitor = None
        self.status = AgentStatus.STOPPED

    def compute_model(self, services: Services):
        """Called periodically, browses queue of data_monitor and request the copy of the files"""
        if not self.__is_data_monitor_running():
            # Agent not started.
            return
        if self.process_file_copy and self.process_file_copy.is_alive():
            # A copy is currently running
            return

        if self.status == AgentStatus.STARTING:
            self.status = AgentStatus.RUNNING

        start = time.perf_counter()
        # poll the queue to retrieve all waiting files
        queue = self.data_monitor.data_monitor_queue
        file_events: List[FileEvent] = []
        while not queue.empty():
            file_events.append(queue.get())
        try:
            if file_events:
                # Creates a process to copy the file
                self.logger.info(f"Copy requested for {len(file_events)} file(s)")
                self.process_file_copy = run_as_process(
                    self.current_parameters, file_events
                )
                self.process_file_copy.start()
                self.data_status.last_modified_date = datetime.datetime.now(tz=pytz.utc)
                self.data_status.file_path = file_events[-1].path

            elapsed = (
                datetime.datetime.now(tz=pytz.utc) - self.data_status.last_modified_date
            )
            self.data_status.status = (
                DataStatusEnum.KO
                if elapsed.seconds > self.current_parameters.datastatus_timeout
                else DataStatusEnum.OK
            )
            t1_stop = time.perf_counter()
            services.get_supervisor_logger().info(
                f"Compute status for {self.name} (took {t1_stop - start}s)"
            )

        except Exception as e:
            self.status = AgentStatus.FAILURE
            self.logger.exception(
                f"An exception occured while trying to copy file {e}"
            )

    def __is_data_monitor_running(self):
        return (
            self.data_monitor is not None
            and self.data_monitor.process_datamonitor is not None
        )
