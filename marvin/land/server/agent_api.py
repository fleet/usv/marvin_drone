import json
from typing import Any

import requests
from requests import HTTPError

from marvin.common import api_path
from marvin.common.api_path import AgentList
from marvin.land.model.data_model import API_TIME_OUT
from marvin.land.parameters import LandParameters


class ApiParser:
    def __init__(self, logger: Any, parameters: LandParameters):
        self.logger = logger
        self.parameters = parameters

    def get_agent_list(self) -> AgentList:
        """
        retrieve list of agents

        :return: The agent list in AgentList structure or None if an connexion error occured
        """
        try:

            # retrieve the list of agents and status
            request_url = (
                f"{self.parameters.drix_api_base}{api_path.supervisor_agent_list}"
            )
            response = requests.get(request_url, timeout=API_TIME_OUT)

            response.raise_for_status()  # raise an exception if an http error occured
            agent_list = AgentList.parse_obj(response.json())  #
            return agent_list
        except HTTPError as e:
            self.logger.warning(f"Http connexion error {e}")
            # keep old status
        except Exception as g:
            self.logger.warning(f"Unknown error while retrieving status {g}")
        return None
