
# create colormap
import numpy as np
from matplotlib.colors import LinearSegmentedColormap

colors = np.array([
          [0,0,0],    # 0x00000000, // noir
     		 [128, 0, 0],	 # 0x00800000, // rouge fonce
     		 [172, 0, 0],	 # 0x00ac0000,
     		 [208, 0, 0],	 # 0x00d00000, // rouge
          [240, 0, 0],   # 0x00f00000, // orange
    		[255, 0, 0],	 #  0x00ff0000, // jaune fonce
    		[255, 121, 121],# 0x00ff7979, // jaune
    		[255, 128, 64], # 0x00ff8040, // jaune clair
    		[255, 128, 0],	 #  0x00ff8000, // vert clair
    		[255, 255, 0],	 # 0x00ffff00, // vert
    		[0, 153, 76],	 # 0x0000ff00, // vert
    		[125, 255, 128],# 0x0080ff80,
    		[0, 0, 153],	 # 0x000080c0, // bleu
    		[0, 128, 255],	 # 0x000080ff, // bleu clair
    		[96, 96, 96],# 0x0080ffff,
    		[192, 192, 192],# 0x00C0C0C0, // gris clair
    		[255, 255, 255],
        ])
colors = np.float16(colors) / 255.
cmap1 = LinearSegmentedColormap.from_list("mycmap", colors[-1:0:-1])



def matplotlib_to_plotly(cmap, pl_entries=255):
    h = 1.0/(pl_entries-1)
    pl_colorscale = []

    for k in range(pl_entries):
        C = np.array(cmap(k*h)[:3])*255
        pl_colorscale.append([k*h, 'rgb'+str((C[0], C[1], C[2]))])

    return pl_colorscale


cmap_plotly = matplotlib_to_plotly(cmap1)
