import datetime
import glob
import time
from pathlib import Path
from threading import Thread
from typing import Tuple, List

import pandas as pd
import numpy as np
import xarray as xr

def parse_transducer_2(dataframe: pd.DataFrame, transducer) -> xr.DataArray:
    # read values from excel file
    # select transducer data
    dfi = dataframe.loc[transducer]
    dfi.set_index(["PingTime"], inplace=True)
    # list the pings indexes
    unique_ping_times = np.unique(dfi.index)
    max_range_dimension = 0
    ping_dimension = len(unique_ping_times)
    retained_range = np.array([0])
    for time_t in unique_ping_times:
        time_values = dfi.loc[time_t]
        rangeData = time_values.RangeStart
        if len(rangeData)>max_range_dimension:
            retained_range=rangeData.values
            max_range_dimension = len(rangeData.values)

    #create storage matrix
    sv = np.full(shape=(ping_dimension,max_range_dimension),fill_value=np.NaN)
    #now store data
    ping_index = 0
    for time_t in unique_ping_times:

        time_values = dfi.loc[time_t]
        rangeData = time_values.RangeStart.values
        sv_values = time_values.Value.values
        sv[ping_index,0:len(rangeData)] = sv_values #fill data by range
        ping_index += 1

    sv = sv / 100.
    unique_ping_times = unique_ping_times.astype(np.datetime64)
    sv_dataset = xr.DataArray(
        data=sv,
        dims=["time","indicative_range"],
        coords={
            "time":unique_ping_times,
            "indicative_range":retained_range
        },
        attrs={
            'description':"Echo integrated sv values"
        }

    )
    return sv_dataset
#
# def parse_transducer(dataframe: pd.DataFrame, transducer) -> xr.DataArray:
#     # read values from excel file, code =Naig Le Bouffant
#     # select transducer data
#     dfi = dataframe.loc[transducer]
#     dfi.set_index(["PingNumber"], inplace=True)
#     data = dfi.to_xarray()
#     # list the pings indexes
#     pingNb = np.unique(data.PingNumber.data)
#     # Ping time
#     PingT = np.unique(data.PingTime.data)
#     PingTime = PingT.astype(np.datetime64)
#
#     # build the Sv echogram
#     first = True
#     Sv = None
#     for pingNbi in pingNb:
#         if first:
#             rangeData = data.sel(PingNumber=pingNbi).RangeStart
#             Sv = np.expand_dims(data.sel(PingNumber=pingNbi).Value.data / 100, 0)
#             first = False
#         else:
#             SvPg = np.expand_dims(data.sel(PingNumber=pingNbi).Value.data / 100, 0)
#             # complete en range si different
#             nr = max(Sv.shape[1], SvPg.shape[1])
#             if nr > Sv.shape[1]:
#                 Sv = np.append(
#                     Sv, np.full((Sv.shape[0], nr - Sv.shape[1]), np.nan), axis=1
#                 )
#                 rangeData = data.sel(PingNumber=pingNbi).RangeStart
#             if nr > SvPg.shape[1]:
#                 SvPg = np.append(SvPg, np.full((1, nr - SvPg.shape[1]), np.nan), axis=1)
#
#             Sv = np.append(Sv, SvPg, axis=0)
#
#
#     # create xarray
#     # Sv=xr.DataArray(Sv,dims=('ping','rangeSv'))
#     # Sv=Sv.assign_coords(ping=pingNb)
#     Sv = xr.DataArray(Sv, dims=("time", "rangeSv"))
#     Sv = Sv.assign_coords(time=PingTime)
#     Sv = Sv.assign_coords(rangeSv=rangeData.data)
#     return Sv


def read_time_values(dataframe: pd.DataFrame, filter_transducer: str) -> xr.Dataset:
    """Read time values data sets for a transducer"""
    dataframe = dataframe.loc[filter_transducer]
    depth = dataframe["BottomDepth"]
    time = dataframe["PingTime"]
    ping_number = dataframe["PingNumber"]
    time_unique, unique_time_index = np.unique(time.to_numpy(), return_index=True)
    depth = depth.to_numpy()[unique_time_index]
    ping_number = ping_number.to_numpy()[unique_time_index]
    data = xr.Dataset(
        data_vars={"depth": (["time"], depth), "ping_number": (["time"], ping_number)},
        coords={"time": time_unique},
    )
    return data


def get_file_list(input_directory: Path, pattern: str):
    """
    Parse directory to retrieve available files matching pattern (like "CW*.csv")
    Result is a sorted list of file (sorted by name, the first supposed to be the latest file)

    """
    file_list = []
    search_pattern = input_directory / pattern
    for file in glob.glob(str(search_pattern)):
        input_file = Path(file)
        if input_file.is_file():
            # found a csv file in directory
            #        modification_time = input_file.stat().st_mtime

            file_list.append(input_file)
    return np.sort(file_list)[::-1]


def do_read_csv(filename: Path) -> Tuple[datetime.datetime, datetime.datetime, pd.DataFrame]:
    """
    Open csv file, read time metadata and return dataframe
    :return
    max_date the max date value
    min_data the min data value
    df a dataframe containing the dataset
    """
    df = pd.read_csv(filename, skiprows=0, sep=";")

    df["PingTime"] = pd.to_datetime(df["PingTime"])
    max_date = df["PingTime"].max()
    min_date = df["PingTime"].min()

    return max_date, min_date, df

def get_transducer_list(df:pd.DataFrame):
    # list transducers
    if df.index.name != "ChannelId":
        raise NotImplementedError("Dataframe is suppposed to be indexed by channelId")
    liste_Tsd = np.unique(df.index.values)
    liste_Tsd = np.sort(liste_Tsd)
    return liste_Tsd

def do_read_dir(input_directory: Path, pattern: str,time_span_second):
    input_files = get_file_list(input_directory=input_directory, pattern=pattern)
    if len(input_files)==0:
        raise FileNotFoundError(f"No file data to merge found in directory {input_directory} with pattern filter {pattern}")
    print("merging datasets")
    merge_df,merged_files = do_merge_datasets(file_list=input_files,time_span_second=time_span_second)
    transducers=get_transducer_list(df=merge_df)

    #read first transducer
    print("reading time values")
    global_data = read_time_values(dataframe=merge_df, filter_transducer=transducers[0])
    #parse transducers
    print('parsing transducers')
    transducers_data = {}
    transducers_data2 = {}
    for transducer_name in transducers:
        print(f"parsing transducer {transducer_name}")
        start = time.time()
        transducers_data[transducer_name] = parse_transducer_2(dataframe=merge_df, transducer=transducer_name)
        elapsed_2 = time.time() - start
        # start = time.time()
        # transducers_data[transducer_name] = parse_transducer(dataframe=merge_df, transducer=transducer_name)
        # elapsed = time.time() - start
        # print(f"parse_transducer new {elapsed_2} vs {elapsed}s {elapsed_2-elapsed}" )

    return transducers_data, global_data


def do_merge_datasets(file_list: List[Path], time_span_second) -> pd.DataFrame:
    """Read all csv file in directory matching pattern
    :param input_directory: the input directory
    :param pattern : a file pattern like "CW.csv"
    :param time_span_second : the number of second to read, if negative all data will be read
    :return a merge dataframe containing all values
    """
    merger_df = None
    newest_date = None
    merged_file = []
    for file in file_list:
        print(f"parsing file {file}")
        merged_file.append(file)
        max_date, min_date, df = do_read_csv(file)
        if newest_date is None:
            newest_date = max_date
        if merger_df is None:
            merger_df = df
        else:
            merger_df = pd.merge(merger_df,df, how = "outer")
        if time_span_second > 0 and min_date < newest_date - pd.Timedelta(
            seconds=time_span_second
        ):
            break
            # stop reading file
    # now filter data on time
    if time_span_second>0:
        start_date = newest_date - pd.Timedelta(seconds=time_span_second)
        merger_df = merger_df[merger_df["PingTime"] >= start_date]
    merger_df.set_index(["ChannelId"], inplace=True)

    return merger_df, merged_file



def do_read(filename, time_span_second):
    # Read the csv file

    start = time.time()
    df = pd.read_csv(filename, skiprows=0, sep=";")
    elapsed = time.time() - start
    print(f"Parsed csv file {filename} in {elapsed}s")

    start = time.time()
    if time_span_second > 0:
        df["PingTime"] = pd.to_datetime(df["PingTime"])
        max_date = df["PingTime"].max()
        min_date = max_date - pd.Timedelta(seconds=time_span_second)
        df = df[df["PingTime"] >= min_date]
    # list transducers
    liste_Tsd = np.unique(df["ChannelId"])
    liste_Tsd = np.sort(liste_Tsd)
    elapsed = time.time() - start
    df.set_index(["ChannelId"], inplace=True)
    print(f"Filter on time criteria in {elapsed}s")
    time_values = read_time_values(dataframe=df, filter_transducer=liste_Tsd[0])

    return {
        transducer_name: parse_transducer_2(dataframe=df, transducer=transducer_name)
        for transducer_name in liste_Tsd
    }, time_values


class CustomReadThread(Thread):
    def __init__(self, filename, time_span_second):
        Thread.__init__(self)
        self.filename = filename
        self.time_span_second = time_span_second
        self.transducer_values = {}
        self.depths_values = None

    def run(self):
        start = time.time()
        print(f"Start of parsing file {self.filename}")
        self.transducer_values, self.depths_values = do_read(
            self.filename, self.time_span_second
        )
        elapsed = time.time() - start
        print(f"End of parsing file {self.filename} in {elapsed}s")


class SampleParser:
    def __init__(self, file_name: Path, time_span_seconds):
        self.file_name = file_name
        self.loaded_time_span = time_span_seconds
        self.thread = CustomReadThread(self.file_name, self.loaded_time_span)

    def get_data_times(self):
        # get one data
        max_times = []
        min_times = []
        transducer_values = self.thread.transducer_values
        for name, data in transducer_values.items():
            max_times.append(data.time.max())
            min_times.append(data.time.min())
        if len(min_times) == 0:
            return (np.datetime64("NaT"), np.datetime64("NaT"))
        return min(min_times), max(max_times)

    def get_transducer_list(self):
        transducer_values = self.thread.transducer_values
        return transducer_values.keys()

    def get_transducer_data(self, name):
        transducer_values = self.thread.transducer_values
        return transducer_values[name]

    def get_time_values(self):
        if self.thread is not None:
            return self.thread.depths_values
        return None

    def is_loading(self):
        # check if we are already loading something
        if self.thread is not None and self.thread.is_alive():
            return True
        return False

    def read_file(self, block=False):
        if self.is_loading():
            # wait previous thread to stop
            self.thread.join()
        self.transducer_values = {}
        self.thread = CustomReadThread(self.file_name, self.loaded_time_span)
        self.thread.start()
        if block:
            self.thread.join()

    def set_loaded_time_span(self, seconds_delay):
        self.loaded_time_span = seconds_delay


if __name__ == "__main__":
    input_dir = Path(r"F:\DELMOGE\20230202_20230204")
    do_read_dir(input_directory=input_dir,pattern="CW_DELM_20230203*.csv",time_span_second=-1)
