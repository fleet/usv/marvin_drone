# -*- coding: utf-8 -*-
"""
Created on Fri Jan 13 09:26:51 2023

@author: nlebouff
"""
import pandas as pd
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

#######################
# inputs
#######################
file=r"C:\dev\workspaces\test\ESSTECH-TL-2022-1_20230118_162155_1.csv" #file to read
Svthr=-70 #Sv threshold for display



# create colormap
colors = np.array([
          [0,0,0],    # 0x00000000, // noir
     		 [128, 0, 0],	 # 0x00800000, // rouge fonce
     		 [172, 0, 0],	 # 0x00ac0000,
     		 [208, 0, 0],	 # 0x00d00000, // rouge
          [240, 0, 0],   # 0x00f00000, // orange
    		[255, 0, 0],	 #  0x00ff0000, // jaune fonce
    		[255, 121, 121],# 0x00ff7979, // jaune
    		[255, 128, 64], # 0x00ff8040, // jaune clair
    		[255, 128, 0],	 #  0x00ff8000, // vert clair
    		[255, 255, 0],	 # 0x00ffff00, // vert
    		[0, 153, 76],	 # 0x0000ff00, // vert
    		[125, 255, 128],# 0x0080ff80,
    		[0, 0, 153],	 # 0x000080c0, // bleu
    		[0, 128, 255],	 # 0x000080ff, // bleu clair
    		[96, 96, 96],# 0x0080ffff,
    		[192, 192, 192],# 0x00C0C0C0, // gris clair
    		[255, 255, 255],
        ])
colors = np.float16(colors) / 255.
cmap1 = LinearSegmentedColormap.from_list("mycmap", colors[-1:0:-1])

 # Read the csv file
df = pd.read_csv(file,skiprows=0,sep=';')

# list transducers
liste_Tsd=np.unique(df['ChannelId'])
df.set_index(['ChannelId'],inplace=True)

#
for tsd in liste_Tsd:
    #select transducer data
    dfi=df.loc[tsd]
    dfi.set_index(['PingNumber'],inplace=True)
    data=dfi.to_xarray()

    #list the pings indexes
    pingNb=np.unique(data.PingNumber.data)

    # build the Sv echogram
    first=True
    for pingNbi in pingNb:
        if first:
            rangeData=data.sel(PingNumber=pingNbi).RangeStart
            Sv=np.expand_dims(data.sel(PingNumber=pingNbi).Value.data/100,0)
            first=False
        else:
            SvPg=np.expand_dims(data.sel(PingNumber=pingNbi).Value.data/100,0)
            # complete en range si different
            nr=max(Sv.shape[1],SvPg.shape[1])
            if nr>Sv.shape[1]:
                Sv=np.append(Sv,np.full((Sv.shape[0],nr-Sv.shape[1]),np.nan),axis=1)
                rangeData=data.sel(PingNumber=pingNbi).RangeStart
            if nr>SvPg.shape[1]:
                SvPg=np.append(SvPg,np.full((1,nr-SvPg.shape[1]),np.nan),axis=1)

            Sv=np.append(Sv,SvPg,axis=0)

    #Ping time
    PingT=np.unique(data.PingTime.data)
    PingTime=[np.datetime64(PingTi[:-1]) for PingTi in PingT]

    # create xarray
    #Sv=xr.DataArray(Sv,dims=('ping','rangeSv'))
    #Sv=Sv.assign_coords(ping=pingNb)
    Sv=xr.DataArray(Sv,dims=('time','rangeSv'))
    Sv=Sv.assign_coords(time=PingTime)
    Sv=Sv.assign_coords(rangeSv=rangeData.data)

    #diplay echograms
    plt.figure()
    plt.clf()
    #p=Sv.plot(x='ping',y='rangeSv',cmap=cmap1,vmin=Svthr,vmax=Svthr+36)
    p=Sv.plot(x='time',y='rangeSv',cmap=cmap1,vmin=Svthr,vmax=Svthr+36)
    p.axes.invert_yaxis()
    plt.title(tsd)


plt.show(block =True )
