import uvicorn
from fastapi import FastAPI

from marvin.common.agent.supervisor import Supervisor
from marvin.common.common_parameters import land_host, land_port
from marvin.land.server.agent_configuration import create_land_agent_list

description = """
Land agents supervisor

# Capacities :
* retrieve agent list
* monitor agent agent_status
* manage start, and stop of agents
"""


if __name__ == "__main__":
    app = FastAPI(
        title="Land agents supervisor",
        description=description,
        version="1.0.0",
    )
    sup = Supervisor(create_land_agent_list())
    sup.declare_api(app)
    sup.start_monitoring()

    config = uvicorn.Config(
        app=app, host=land_host, port=land_port, log_level="warning", reload=True
    )
    server = uvicorn.Server(config)
    server.run()
    sup.stop()
