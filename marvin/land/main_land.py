from dash import Dash
from marvin.land import style


from marvin.land.components.layout import create_layout
from marvin.land.model.data_model import DataControler


def main() -> None:
    app = Dash(external_stylesheets=[style.theme])
    app.title = "Drix Ifremer dashboard"
    controler = DataControler()
    controler.start()
    app.layout = create_layout(app, controler)
    app.run()
    controler.stop()


if __name__ == "__main__":
    main()
