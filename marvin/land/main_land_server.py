"""
Work in progress
This is a server component dedicated to retrieve data from drix and publish them in a datamodel (file, database etc)
Now a big part is done in viewer through data_model class
but most should be moved and re-implemented in this

"""
from pathlib import Path

from marvin.land.server.data_server import DataServer


def main() -> None:
    wkdir = Path(__file__).parent.parent.parent / "working_directory"
    if not wkdir.exists():
        raise FileNotFoundError(f"directory {wkdir} does not exist")

    server = DataServer(working_directory=wkdir)
    server.start()

if __name__ == "__main__":
    main()
