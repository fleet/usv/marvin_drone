"""
Loggin interface for land process
"""
import logging

FORMAT = "%(asctime)s %(message)s"
logging.getLogger("werkzeug").setLevel(
    logging.ERROR
)  # remove message POST /_dash-update-component HTTP/1.1 messages
logging.basicConfig(format=FORMAT, level=logging.INFO)


class Logger:
    @staticmethod
    def info(message):
        logging.info(message)

    @staticmethod
    def error(message):
        logging.error(message)

    @staticmethod
    def warning(message):
        logging.warning(message)

    @staticmethod
    def exception(message):
        logging.exception(message)


#       pass
