import abc
import time
from threading import Thread


class ThreadLoop(abc.ABC):


    def __init__(self,refresh_period):
        self._should_stop = False  # control for status update loop
        self.refresh_period=refresh_period
        self.thread = Thread(
            target=self.refresh_loop
        )  # thread for status update control

    def start(self):
        self._should_stop = False
        self.thread.start()

    def stop(self):
        self._should_stop = True
        self.thread.join(timeout=10)

    def refresh_loop(self):
        self._initialize()

        #Run processing for all threads
        while not self._should_stop:
            start = time.time()
            self._periodic_process()
            elapsed = time.time() - start
            wait_time = self.refresh_period - elapsed
            if wait_time > 0:
                time.sleep(wait_time)

    @abc.abstractmethod
    def _initialize(self):
        """
        initialize server dataprocessing, called once before starting processing loop
        """

    @abc.abstractmethod
    def _periodic_process(self):
        """
        Do real periodic processing
        """
