from pathlib import Path

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from marvin.common.common_parameters import drone_host, land_host


class LandParameters(BaseModel):
    # drix server configuration
    drix_ip_adress = drone_host
    drix_api_port = 5000
    drix_api_base = f"http://{drix_ip_adress}:{drix_api_port}"

    land_server = land_host

    # Dashboard configuration
    refresh_rate = 2.0  # refresh rate in sec

    datamodel_refresh_period = 10.0  # refresh rate in sec

    server_refresh_period = 10.0  # refresh rate in sec

    requests_timeout = 10


class HermesParameters(BaseModel):
    input_dir = Path(r"E:\CSWOT\HERMES")

    # input_dir = Path(r'F:\DELMOGE\20230202_20230204')

    auto_refresh_period = 2 * 60
    enable_auto_refresh = True

    time_span_min = 0
    time_span_hour = 1

    datamodel_last_date_timeout = (
        1.2 * auto_refresh_period
    )  # if data are older than this value, warn to the user
