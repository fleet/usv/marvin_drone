from dash_bootstrap_components.themes import BOOTSTRAP, DARKLY
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.data import DataStatusEnum
from marvin.land.model.data_model import AgentStatusModel

# theme = DARKLY
theme = BOOTSTRAP


# datatable style
# style_header = {
#                    'backgroundColor': 'rgb(30, 30, 30)',
#                    'color': 'white'
#                }
style_header = {"backgroundColor": "grey", "color": "black", "fontWeight": "bold"}

# style_data = {
#                  'backgroundColor': 'rgb(50, 50, 50)',
#                  'color': 'white'
#              }
style_data = ({"color": "black", "backgroundColor": "gainsboro"},)

style_cell_conditional = [
    {"if": {"column_id": f"{AgentStatusModel.AGENT_COL}"}, "textAlign": "left"}
]

style_data_conditional = [
    {
        "if": {
            "column_id": f"{AgentStatusModel.STATUS_COL}",
            "filter_query": "{"
            + AgentStatusModel.STATUS_COL
            + "} = "
            + f"{AgentStatus.FAILURE.name}",
        },
        "backgroundColor": "hotpink",
        "color": "black",
        "border": "1px solid rgb(0, 116, 217)",
    },
    {
        "if": {
            "column_id": f"{AgentStatusModel.STATUS_COL}",
            "filter_query": "{"
            + AgentStatusModel.STATUS_COL
            + "} = "
            + f"{AgentStatus.RUNNING.name}",
        },
        "color": "seagreen",
        "fontWeight": "bold",
        "border": "1px solid rgb(0, 116, 217)",
    },
    {
        "if": {
            "column_id": f"{AgentStatusModel.DATA_COL}",
            "filter_query": "{"
            + AgentStatusModel.DATA_COL
            + "} = "
            + f"{DataStatusEnum.OK.name}",
        },
        "color": "seagreen",
        "fontWeight": "bold",
        "border": "1px solid rgb(0, 116, 217)",
    },
    {
        "if": {
            "column_id": f"{AgentStatusModel.DATA_COL}",
            "filter_query": "{"
            + AgentStatusModel.DATA_COL
            + "} = "
            + f"{DataStatusEnum.KO.name}",
        },
        "backgroundColor": "hotpink",
        "color": "black",
        "border": "1px solid rgb(0, 116, 217)",
    },
]
