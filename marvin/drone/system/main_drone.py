import uvicorn
from fastapi import FastAPI

import marvin.drone.system.agent_configuration as drone_agent_configuration
from marvin.common.agent.supervisor import Supervisor
from marvin.common.common_parameters import drone_host, drone_port

# create fast api rest bindings
from marvin.common.utils.parameters import pretty_json

description = """
Marvin drone supervisor

# Capacities :
* retrieve agent list
* monitor agent agent_status
* manage start, and stop of agents

"""


if __name__ == "__main__":
    app = FastAPI(
        title="Drone agents supervisor",
        description=description,
        version="1.0.0",
    )
    sup = Supervisor(drone_agent_configuration.create_drone_agent_list())
    sup.declare_api(app)
    sup.start_monitoring()

    config = uvicorn.Config(
        app=app, host=drone_host, port=drone_port, log_level="warning", reload=True
    )
    server = uvicorn.Server(config)
    server.run()
    sup.stop()
