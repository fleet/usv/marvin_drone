"""
Manage a list of services used to centralize and avoid too many concurrent computation

"""
from typing import Any

from marvin.drone.system import common_logger
from marvin.drone.system.services.process_services import ProcessMonitor


class Services:
    _services = {}

    def __init__(self):
        # declare common services
        self._services[ProcessMonitor.NAME] = ProcessMonitor()
        self.logger = common_logger.configure_console()

    def declare_service(self, name, value):
        self._services[name] = value

    def get_service(self, name: str) -> Any:
        return self._services[name]


    def get_supervisor_logger(self):
        return self.logger
