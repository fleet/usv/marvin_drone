"""
Service retrieving process list and caching returned values to avoid too many system access

"""
from marvin.common.agent.parameters import ProcessMonitorParameter
from marvin.common.processes import get_processes_by_name


class ProcessMonitor:
    NAME = "ProcessMonitor"

    def __init__(self):
        self.cache_timeout = ProcessMonitorParameter.cache_timeout

    def is_process_alive(self, name) -> bool:
        """
        Indicate if at least one process of that name is alive
        """
        process_list = get_processes_by_name(name=name)
        return len(process_list) > 0
