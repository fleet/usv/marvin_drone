from pathlib import Path
from typing import Dict

from marvin.drone.adcp.rdi.rdi_agent import RdiAgent
from marvin.drone.ek80.ek80_agent import EK80Agent
from marvin.drone.hydrophone.hydro_agent import HydrophoneAgent
from marvin.common.agent.agent import Agent


def create_drone_agent_list() -> Dict[str, Agent]:
    adcp = RdiAgent(working_directory=Path.cwd().parent.parent.parent / "working_directory")
    ek80 = EK80Agent(working_directory=None, agent_id="ek80")
    #hydro = HydrophoneAgent(working_directory=None, agent_id="hydrophone")


    return {
         adcp.agent_id: adcp,
         ek80.agent_id: ek80,
#        hydro.agent_id: hydro,
    }
