import logging
import logging.handlers as hd
from pathlib import Path
from marvin.common import common_logger
FORMAT = "%(asctime)s %(message)s"

def configure_console():
    return common_logger.configure_console()


def configure(prefix: str, logger_name: str = None):
    """Configure default logger and add a file handler"""
    log_dir = Path(__file__).parent.parent.parent.parent
    log_dir = log_dir / "logs"
    return common_logger.configure(prefix=prefix,log_directory=log_dir,logger_name=logger_name)
