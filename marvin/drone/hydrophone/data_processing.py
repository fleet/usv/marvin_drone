"""
Handle in a separate process the data processing of hydrophone
Namely :
    retrieve data from hydrophone, delete copied data
    compute summary of retrieved data
    publish it to network
"""
import datetime
import functools
import time
from ftplib import Error
from multiprocessing import Process
from os import PathLike
from pathlib import Path
from typing import Union, Any

import pika
import pytz
import xarray as xr

from marvin.common.hydrophone_api import MessageBroker, MessageEncoder
from marvin.drone.hydrophone.data_processing_parameters import Parameters
from marvin.drone.hydrophone.ftp_retriever import FTPRetriever
from marvin.drone.hydrophone.spectrogram_generator import wav2spectrogram, spectrogram2xarray
from marvin.drone.system import common_logger


def __run(parameters: Parameters):
    instance = DataProcessor(parameters)
    instance.run()


def run_as_process(parameters) -> Process:
    """ Create a DataProcessor and execute run method on it"""
    return Process(target=functools.partial(__run, parameters))


class DataPublisher:
    """Publish a xarray sampled dataset to the message broker

    """

    def __init__(self, logger):
        self.logger = logger
        self.connection = None
        self.channel = None

    def connect(self):
        if not self.connection or self.connection.is_closed:
            credentials = pika.credentials.PlainCredentials(username="marvin", password="marvin")
            self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=MessageBroker.broker_host, credentials=credentials))
            self.channel = self.connection.channel()
            self.channel.exchange_declare(exchange=MessageBroker.exchange_name,
                                          exchange_type=MessageBroker.exchange_type)

    def __publish(self, message):
        """create data producer"""
        self.channel.basic_publish(exchange=MessageBroker.exchange_name, routing_key='', body=message)

    def publish(self, message):
        try:
            if self.connection is None:
                self.connect()
            self.__publish(message=message)
        except pika.exceptions.ConnectionClosed:  # connection closed, might be due to heartbit timeout
            self.logger.info("RabbitMQ Connection closed, reconnecting")
            self.connect()
            self.publish(message=message)

    def close(self):
        if self.connection is not None and not self.connection.is_closed:
            self.connection.close()


class DataProcessor:
    def __init__(self, parameters: Parameters):
        self.publisher = None
        self.parameters = parameters
        self.logger = common_logger.configure(prefix="hydrophone_file_retriever",
                                              logger_name="hydrophone_file_retriever")

    def __del__(self):
        if self.publisher is not None:
            self.publisher.close()

    def run(self):
        """
        Execute data processing
        """

        retriever = FTPRetriever(parameters=self.parameters, logger=self.logger)

        self.publisher = None
        if self.parameters.publish_active:
            try:
                self.publisher = DataPublisher(logger=self.logger)
                self.publisher.connect()
            except Exception as e:
                self.logger.warning(f"A message broker connection error occured, continuing {e}")
                self.publisher = None


        while True:
            start = time.time()
            try:
                retriever.retrieve_files2(notify=functools.partial(process_new_data, publisher=self.publisher, sampler_parameter=self.parameters.data_sampling_parameters))
            except (Error, OSError, EOFError) as e:
                # ftp exception, might not be a programming error continue processing loop
                self.logger.exception(e)
            except Exception as e:
                self.logger.exception(e)
                raise e
            elapsed = time.time() - start
            wait_time = self.parameters.period - elapsed
            if wait_time > 0:
                time.sleep(wait_time)


def extract_file_name_starting_date(filename: Union[str, PathLike]) -> datetime.datetime:
    """
    From an hydrophone file name compute starting date
    :param filename: the name of the file like /home/SBW1597_20100101_010256.wav

    """
    test_name = Path(str(filename))

    test_name = test_name.stem  # remove .wav and directory
    test_name_list = test_name.rsplit("_")
    test_name = "_".join(test_name_list[1::])  # remove cardinal
    starting_date = datetime.datetime.strptime(test_name, format("%Y%m%d_%H%M%S"))
    starting_date = starting_date.replace(tzinfo=pytz.utc)
    return starting_date


def extract_spectrogram(file_path: Union[str, Path]) -> xr.Dataset:
    """Extract spectrogram data from a .wav file"""
    file = Path(str(file_path))
    if not file.exists():
        raise FileNotFoundError(f"spectrogram computation failed : file {file} does not exists")
    starting_date = extract_file_name_starting_date(filename=file)
    xr_dataset = wav2spectrogram(filename=file, starting_date=starting_date)
    return xr_dataset


def process_new_data(file_name: str, logger: Any, publisher: DataPublisher, sampler_parameter:str):
    """ Process data when a new wav file is retrieved"""
    file_path = Path(file_name)
    start = time.time()

    data = extract_spectrogram(file_path=file_path)
    # Resample data

    data = data.resample(time=sampler_parameter).median()

    logger.info(f"Extract spectrogram for {file_path} : took {time.time() - start}s")
    #try to write file as netcf file
    try:

        data.to_netcdf(file_path.with_suffix(".nc"))
    except Exception as e:
        logger.error(f"Cannot write netcdf file {file_path.with_suffix('.nc')} to disk, ignoring error")

    # save data to disk and/or publish data to land
    if publisher is not None:
        publisher.publish(MessageEncoder.encode(file_name=file_path, dataset=data))
    # start = time.time()
    # figure_path = file_path.parent / (file_path.name + ".png")
    # save_figure(spectogram=data, file_name=figure_path)
    # logger.info(f"Save figure {figure_path} : took {time.time() - start}s")
