from pydantic import BaseModel  # pylint: disable=no-name-in-module
from .data_processing_parameters import Parameters as pftp


# from .secret.ftp_test_parameters import Parameters as pftp


class AgentParameters(BaseModel):
    kill_timeout: float = 20
    data_directory = pftp.local_directory
    data_pattern = ["*.wav"]
    file_monitor_period = 60
    datastatus_timeout = 60 * 4  # if no data was produced or modified since that time, data status is marqued as in error
