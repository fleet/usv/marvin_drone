from pathlib import Path


class Parameters:
    site = "192.168.1.30"
    login = ""
    passwd = ""
    distant_directory = "Data"
    local_directory = Path(r"E:\HYDROPHONE_DATA")
    file_filter = "*.wav"
    retention_time_s = 3*60.0  # we leave data newer than the retention_time
    delete_after_copy = True
    period = 30  # period for looping
    generate_figures = True

    data_sampling_parameters = "1S"
    publish_active = False

default_parameters = Parameters()
