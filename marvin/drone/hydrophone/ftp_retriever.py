"""
Copy files and make space from hydrophone ftp site
Hydro deliver its data through a ftp site, once copied they will be erased from hydrophone to free some space

"""
import datetime
import fnmatch
from ftplib import FTP
# from marvin.drone.hydrophone.parameters import Parameters
from typing import Callable, List, Any

from marvin.drone.hydrophone.data_processing_parameters import Parameters
from marvin.drone.hydrophone.pyFTPclient.pyftpclient import PyFTPclient


#pylint: disable=unused-argument
def empty_notif(file, logger):
    pass

class FTPRetriever:
    def __init__(self, parameters:Parameters, logger:Any):
        # set Logging to file with default handler
        self.parameters = parameters
        self.logger = logger


    def delete_file(self,file) -> None:
        with FTP(
            host=self.parameters.site, user=self.parameters.login, passwd=self.parameters.passwd  # one hour timeout
        ) as ftp:  # connect to host, default port
            ftp.set_pasv(True)
            ftp.cwd(dirname=self.parameters.distant_directory)
            ftp.delete(filename=file)
            self.logger.info(f"FTP file : {file} was deleted")


    def get_file_list(self) -> List[str]:
        final_list=[]
        with FTP(
            host=self.parameters.site, user=self.parameters.login, passwd=self.parameters.passwd  # one hour timeout
        ) as ftp:  # connect to host, default port
            ftp.set_pasv(True)
            ftp.cwd(dirname=self.parameters.distant_directory)
            file_list = ftp.nlst()
            files_times = {}
            for file in file_list:
                if fnmatch.fnmatch(file,self.parameters.file_filter):
                    timestamp = ftp.voidcmd(f"MDTM {file}")[4:].strip()
                    file_date = datetime.datetime.strptime(
                        timestamp,
                        "%Y%m%d%H%M%S",
                    )
                    nt = datetime.datetime.astimezone(
                        file_date, tz=datetime.timezone.utc
                    )  # to check depending on hydrophone timezone
                    files_times[file] = file_date

            sorted_files = sorted(files_times.items(), key=lambda x: x[1])
            if len(sorted_files) != 0:
                # remove last = newest file
                sorted_files.remove(sorted_files[-1])
                # Filter file to retrieve only the ones older than x hour
                for file, file_time in sorted_files:
                    if file_time < datetime.datetime.now() - datetime.timedelta(
                        seconds=self.parameters.retention_time_s
                    ):
                        final_list.append(file)

            if len(final_list) > 0:
                list_str = " ".join(final_list)
                self.logger.info(f"FTP file list to process {list_str}")
            else:
                self.logger.info(f"No data matching retrieval criteria")
        return final_list

    def retrieve_files2(self, notify: Callable[[str,Any], None] = empty_notif) -> None:
        """
        Connect to ftp site and retrieve all avalaible files based except the last one
        if setted in parameters, files are erased after modifications

        :param parameters: the ftp parameters
        :notify notify: a notification callback allowing to start post processing when a file is newly added and available
        :return: None
        """
        # open connection to ftp
        file_list = self.get_file_list()
        #use pyftpclient to download data

        for file in file_list:
            output_file = self.parameters.local_directory / file
            obj = PyFTPclient(self.parameters.site, port=21, login=self.parameters.login, passwd=self.parameters.passwd, logger = self.logger)
            dst_filename = self.parameters.distant_directory + "/" + file
            ret = obj.DownloadFile(dst_filename=dst_filename,local_filename=output_file)
            if ret is not None and ret == 1:
                self.logger.info(f"Downloaded {dst_filename} to {output_file}")
                #now we need to delete file after copy
                self.delete_file(file=file)
                notify(output_file, self.logger)  # notify that a new file is avalaible for post processing


#
# def retrieve_files(parameters: Parameters, notify: Callable[[str,Any], None] = empty_notif) -> None:
#     """
#     Connect to ftp site and retrieve all avalaible files based except the last one
#     if setted in parameters, files are erased after modifications
#
#     :param parameters: the ftp parameters
#     :notify notify: a notification callback allowing to start post processing when a file is newly added and available
#     :return: None
#     """
#     # open connection to ftp
#
#     with FTP(
#         host=parameters.site, user=parameters.login, passwd=parameters.passwd, timeout=60 * 60  # one hour timeout
#     ) as ftp:  # connect to host, default port
#         ftp.set_pasv(True)
#         logger.info(f"Connected to {Parameters.site}")
#         ftp.cwd(dirname=parameters.distant_directory)
#         file_list = ftp.nlst()
#         files_times = {}
#         for file in file_list:
#             if fnmatch.fnmatch(file, parameters.file_filter):
#                 timestamp = ftp.voidcmd(f"MDTM {file}")[4:].strip()
#                 file_date = datetime.datetime.strptime(
#                     timestamp,
#                     "%Y%m%d%H%M%S",
#                 )
#                 nt = datetime.datetime.astimezone(
#                     file_date, tz=datetime.timezone.utc
#                 )  # to check depending on hydrophone timezone
#                 files_times[file] = file_date
#
#         sorted_files = sorted(files_times.items(), key=lambda x: x[1])
#         if len(sorted_files) == 0:
#             logger.info(f"No data matching retrieval criteria")
#             return
#         # remove last = newest file
#         sorted_files.remove(sorted_files[-1])
#         # Filter file to retrieve only the ones older than x hour
#         for file, file_time in sorted_files:
#             if file_time < datetime.datetime.now() - datetime.timedelta(
#                 seconds=parameters.retention_time_s
#             ):
#                 output_file = parameters.local_directory / file
#                 try:
#                     distant_file_size = ftp.size(file)
#                     logger.info(
#                         f"FTP downloading file {file} : ({distant_file_size} bytes)"
#                     )
#                     with open(output_file, "wb") as fp:
#                         ret = ftp.retrbinary(f"RETR {file}", fp.write)
#                         logger.info(
#                             f"FTP downloading file {file} : ({distant_file_size} bytes)"
#                         )
#                     logger.info(
#                         f"file {file} downloaded : ({distant_file_size} bytes)"
#                     )
#                     # no errors remove file on distant directory
#                     # parse returned value :
#                     error = True
#                     try:
#                         code = ret[:3]
#                         code = int(code)
#                         if 200 <= code < 300:
#                             error = False
#                     except Exception as e:
#                         logger.error("Cannot parse ftp return code : {ret}")
#                         error = True
#                     # check local file size
#                     local_size = os.path.getsize(output_file)
#                     if local_size != distant_file_size:
#                         logger.error(
#                             f"FTP File size differs between distant and local file : {distant_file_size} vs {local_size}"
#                         )
#                         error = True
#                     if not error:
#                         if parameters.delete_after_copy:
#                             ftp.delete(filename=file)
#                             logger.info(f"FTP Delete file : {file}")
#                         notify(output_file)  # notify that a new file is avalaible for post processing
#
#                 except OSError as e:
#                     logger.error(f"An os error occured while writing file {file} : {e}")
#                 except (error_perm, error_reply, Error) as e:
#                     logger.error(
#                         f"A FTP error occured for file {file} : error code  = {e}"
#                     )
