import time
from pathlib import Path

from marvin.common import api_path as paths
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.file_watcher import DataStatusMonitor
from marvin.drone.hydrophone.data_processing import run_as_process
from marvin.drone.hydrophone.data_processing_parameters import default_parameters
from marvin.drone.hydrophone.parameters import AgentParameters
from marvin.drone.system import common_logger
from marvin.common.agent.agent import Agent
from marvin.drone.system.services.services import Services


class HydrophoneAgent(Agent):
    def __init__(self, working_directory: Path, agent_id=paths.rdi_agent_id):
        super().__init__(name="Hydrophone data retrieval Agent", agent_id=agent_id)
        self.working_directory = working_directory
        self.current_parameters = AgentParameters()
        self.process_file_copy = None  # id of the python process retrievinng files
        self.process_datamonitor = None  # id of the python monitoring data production
        self.logger = common_logger.configure("Hydrophone Agent", logger_name="Hydro")

        self.data_monitor = DataStatusMonitor(
            directories=[Path(self.current_parameters.data_directory)],
            filter_pattern=self.current_parameters.data_pattern,
            period_s=self.current_parameters.file_monitor_period,
        )

    def compute_model(self, services: Services):
        # compute data production status
        start = time.perf_counter()
        self.data_monitor.compute_datastatus(
            datastatus_timeout=self.current_parameters.datastatus_timeout
        )
        self.data_status = self.data_monitor.data_status
        # compute agent status

        # check if file production and copy in hydrophone
        if self.process_file_copy is None:
            self.status = AgentStatus.STOPPED
        elif self.process_file_copy.is_alive():
            self.status = AgentStatus.RUNNING
        else:
            # process should never return, if so it is in failure
            self.status = AgentStatus.FAILURE

        t1_stop = time.perf_counter()
        services.get_supervisor_logger().info(
            f"Compute status for {self.name} (took {t1_stop - start}s)"
        )

    def start(self):
        self.status = AgentStatus.STARTING
        if self.process_file_copy is not None:
            self._stop()
        try:

            self.process_file_copy = run_as_process(parameters=default_parameters)
            self.process_file_copy.start()
            # start process allowing data retrieval from ftp hydrophone site

        except Exception as e:
            self.status = AgentStatus.FAILURE
            self.logger.exception(
                f"An exception occured while trying to start agent {e}"
            )

    def stop(self):
        try:
            self._stop()
            self.status = AgentStatus.STOPPED

        except Exception as e:
            self.logger.exception(
                f"An exception occured while trying to start {self.name} {e}"
            )
            self.status = AgentStatus.FAILURE

    def _stop(self):
        if self.process_file_copy is not None:
            self.process_file_copy.terminate()
            self.process_file_copy.join(timeout=self.current_parameters.kill_timeout)
            if self.process_file_copy.exitcode is None:
                self.process_file_copy.kill()
            self.process_file_copy = None
