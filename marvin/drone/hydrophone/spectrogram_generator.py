import datetime
from pathlib import Path
from typing import Union

#import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.io import wavfile
import xarray as xr

class XrStruct:
    """Variables definition for xrarray structure """
    frequency = "frequency"
    time = "time"
    data = "spectrogram"
    source_att = "source_file"
    version_att = "version"
    description_att = "description"

def wav2spectrogram(filename:Path, starting_date:datetime.datetime) ->xr.Dataset:
    """
        From a .wav file generate a spectrogram as an xarray dataset
    """
    frequencies, dates, spectrogram = compute_spectrogram(filename=filename, starting_date=starting_date)
    return spectrogram2xarray(frequencies=frequencies, times=dates, values=spectrogram)

def compute_spectrogram(filename: Path, starting_date: datetime.datetime):
    """
    From a .wav file generate a spectrogram
    """

    sample_rate, samples = wavfile.read(str(filename))
    frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate)
    #convert time to time_delta, to prevent rounding to integer number of second we work as micro seconds
    times_delta = (times*1000*1000).astype('timedelta64[us]')
    return frequencies, np.datetime64(starting_date)+times_delta, spectrogram


# def save_figure(spectogram:xr.Dataset,file_name:Union[str,Path]):
#     file_name=Path(str(file_name))  #ensure we got a path object
#     v = spectogram.transpose( XrStruct.frequency,XrStruct.time)  # ensure axis places for graphics, when decimating order can change
#     v= np.log(v[XrStruct.data]) #switch to db to have something readable
#     fig=plt.figure()
#     v.plot() #plt data
#     fig.savefig(file_name)#save to file
#     plt.close(fig=fig) #close figure

def spectrogram2xarray(frequencies, times, values):
    """
    Convert spectogram data to xarray
    """
    dataset= xr.Dataset(
        data_vars={
            XrStruct.data:([XrStruct.frequency,XrStruct.time], values),
        },
        coords={
            XrStruct.time: times,
            XrStruct.frequency : frequencies,
        },
        attrs={
            XrStruct.description_att:"Spectrogram",
            XrStruct.version_att:"0.1",
            XrStruct.source_att:"Unknown"
            }
    )
    dataset[XrStruct.frequency].attrs["units"] = "Hz"

    return dataset
