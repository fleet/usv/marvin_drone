import json
from os import PathLike
from pathlib import Path

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from marvin.common.utils import parameters as param_utils


class AgentParameters(BaseModel):

    # serial port configuration
    baudrate: int = 9600 # 38400
    serial_port: str = "COM8"
    break_time: float = 0.4*2  # time duration for a break
    time_to_wait_s: float = 1  # time to wait after sending all commands
    inter_command_wait_s: float = 0.1  # time to wait after between two commands

    # vmdas parameters
    vmdas_exe: str = r"C:/Program Files (x86)/RD Instruments/VmDas/VmDas.exe"
    vmdas_process_name: str = "vmdas.exe"
    vmdas_process_param: str = r"/autostart"
    vmdas_output_directory: str = r"D:/Data/ADCP"

    vmdas_data_pattern = ["*.STA", "*.ENX"] #list of file extension to monitor

    file_monitor_period = 0.5 * 60 #recurrence for file monitoring period
    datastatus_timeout = 1 * 60 #timeout for file monitoring period

    # BBTalk configuration files
    cmd_file_dir: str = "adcp/rdi/cmd_files"
    default_command_file: str = "default.txt"

    # RDI controler configuration
    # port = 5010

class RDIState(BaseModel):
    """
    Serialize RDI agent current state to be able to retrieve it at startup
    """
    last_command_file: str = "default.txt"

    @staticmethod
    def get_parameter_path(working_directory:Path):
        """return parameters file name for parameters"""
        return working_directory / "adcp" / "rdi" / "rdistate.json"



    def write_tojson(self, filename: PathLike):
        param_utils.write_tojson(config=self, filename=filename)

    @staticmethod
    def read_fromjson(filename: PathLike):
        """Read configuration from json file"""
        return RDIState.parse_file(filename)

