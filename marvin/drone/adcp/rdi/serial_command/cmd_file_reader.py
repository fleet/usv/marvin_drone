import time
from os import PathLike

from marvin.drone.adcp.rdi.serial_command.serial_interface import Communicator


class CmdReader:
    """
    Send command to serial ADCP from a text file

    """

    def __init__(self, communicator: Communicator, inter_cmd_pause=0):
        self.COMMENT_MARKER = ";"
        self.communicator = communicator
        self.inter_cmd_pause=inter_cmd_pause #time to wait between two commands

    def read(self, filename: PathLike):
        # Using for loop
        with open(filename, "r", encoding="utf8") as file:
            for line in file:
                line = line.strip()  # remove \n
                # remove comments
                line_split = line.split(self.COMMENT_MARKER)
                line = line_split[0].strip()
                line = line.strip()
                if len(line) > 0:
                    # send data to
                    if self.inter_cmd_pause > 0:
                        time.sleep(self.inter_cmd_pause)
                    self.communicator.write(line)



#
#
# if __name__ == "__main__":
#     # pylint: disable=wrong-import-position,redefined-outer-name
#     import os
#     import time
#     import drone.adcp.rdi.serial_command.serial_interface as ser
#
#     with ser.open_communicator(ser.SerialRDI(baudrate=parameters.baudrate, port=parameters.port)) as com:
#         reader = CmdReader(communicator=com)
#         reader.communicator.send_break(duration_sec=parameters.break_time)
#         filename2 = os.path.realpath(os.path.abspath("../../tests/ADCP_DrixLight.txt"))
#         reader.read(filename2)
#         time.sleep(parameters.time_to_wait_s)
#         # assume that everything went well
#     print("End of communication")
