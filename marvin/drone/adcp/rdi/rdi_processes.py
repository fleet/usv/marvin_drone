import os
import time
from pathlib import Path

from marvin.drone.adcp.rdi import parameters
import marvin.drone.adcp.rdi.serial_command.serial_interface as ser
from marvin.drone.adcp.rdi.serial_command.cmd_file_reader import CmdReader
from marvin.common import processes


def stop_pinging(current_parameter: parameters.AgentParameters):
    """Open ADCP port and send a break to stop pinging"""
    with ser.open_communicator(
        ser.SerialRDI(
            baudrate=current_parameter.baudrate, port=current_parameter.serial_port
        )
    ) as com:
        reader = CmdReader(communicator=com, inter_cmd_pause=current_parameter.inter_command_wait_s)
        reader.communicator.send_break(duration_sec=current_parameter.break_time)


def load_configuration(
    filename: Path,
    working_directory: Path,
    current_parameter: parameters.AgentParameters,
    rdi_state: parameters.RDIState,
    save_as_defaut=True,
):
    """
    Open serial port and load default configuration file
    This will block until complete
    """

    if not os.path.exists(filename):
        raise FileNotFoundError(f"Default configuration file does not exists")

    with ser.open_communicator(
        ser.SerialRDI(
            baudrate=current_parameter.baudrate, port=current_parameter.serial_port,
        )
    ) as com:
        reader = CmdReader(communicator=com, inter_cmd_pause=current_parameter.inter_command_wait_s)
        reader.communicator.send_break(duration_sec=current_parameter.break_time)
        reader.read(filename)
        if current_parameter.time_to_wait_s > 0:
            time.sleep(current_parameter.time_to_wait_s)

    if save_as_defaut:
        # save current configuration as default, in case of reboot we will send the last one
        rdi_state.last_command_file = filename


#       agent_parameter_file = parameters.get_parameter_path(working_directory)
#       parameters.write_tojson(current_parameter, filename=agent_parameter_file)


def load_default_rdi_config(
    working_directory: Path, current_parameter: parameters.AgentParameters, rdi_state: parameters.RDIState
):
    """
    Open serial port and load default configuration file
    This will block until complete
    """
    cmd_file = Path(rdi_state.last_command_file)

    if not cmd_file.exists():
        cmd_file = (
            working_directory
            / current_parameter.cmd_file_dir
            / current_parameter.default_command_file
        )
    load_configuration(
        filename=cmd_file,
        working_directory=working_directory,
        current_parameter=current_parameter,
        rdi_state=rdi_state,
        save_as_defaut=False,
    )


def start_acquisition(current_parameter: parameters.AgentParameters):
    """Start vmdas acquisition process as a separate process"""
    #cmd line
 #   cmd_line = f'{current_parameter.vmdas_exe} {current_parameter.vmdas_process_param}'
#  os.system(cmd_line)
    processes.spawn_process(
        current_parameter.vmdas_exe, [current_parameter.vmdas_process_param]
    )

