import time
from pathlib import Path
from typing import Union

from fastapi import FastAPI, APIRouter, HTTPException
from pydantic import BaseModel
from marvin.common import api_path as paths
from marvin.common.api_path import ConfigParameter
from marvin.common.file_watcher import DataStatusMonitor
from marvin.drone.adcp.rdi import rdi_processes
from marvin.drone.adcp.rdi import parameters as config
from marvin.drone.adcp.rdi.parameters import AgentParameters, RDIState
from marvin.drone.system import common_logger
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.processes import kill_process
from marvin.common.agent.agent import Agent, AgentIds
from marvin.drone.system.services.process_services import ProcessMonitor
from marvin.drone.system.services.services import Services



class RdiAgent(Agent):
    def __init__(self, working_directory: Path, agent_id=paths.rdi_agent_id):
        super().__init__(name=AgentIds.adcp_drone, agent_id=agent_id)
        self.working_directory = working_directory
        self.logger = common_logger.configure(
            prefix="RDI ADCP Agent", logger_name="RDIAgent"
        )

        self.current_parameters = AgentParameters()
        self.rdi_state = RDIState()
        self.data_monitor = DataStatusMonitor(
            directories=[Path(self.current_parameters.vmdas_output_directory)],
            filter_pattern=self.current_parameters.vmdas_data_pattern,
            period_s=self.current_parameters.file_monitor_period,
        )

    def start(self):
        self.status = AgentStatus.STARTING
        self.logger.info(f"{self.name} starting")

        self.logger.info(f"{self.name} stopping previous instances of vmdas")
        try:
            self._stop()
        except Exception as e:
            self.logger.error(f"An exception occured while trying to stop ADCP : {e}")
        # TODO : save the previous configuration as default configuration
        try:
            self.status = AgentStatus.STARTING
            #self.logger.info(f"{self.name} loading default configuration")
            #rdi_processes.load_default_rdi_config(
            #    working_directory=self.working_directory,
            #    current_parameter=self.current_parameters,
            #    rdi_state=self.rdi_state
            #  )
            self.logger.info(f"{self.name} starting acquisition")
            rdi_processes.start_acquisition(current_parameter=self.current_parameters)

            self.status = AgentStatus.RUNNING
            self.logger.info(f"{self.name} running ")
        except Exception as e:
            self.logger.exception(
                f"An exception occured while trying to start ADCP {e}"
            )
            self.status = AgentStatus.FAILURE

    def compute_model(self, services: Services):
        """
        Function called periodically in the main loop to ensure heavy weight status retrieval and computations
        """
        #check if vmdas process is alive
        start = time.perf_counter()
        process_service = services.get_service(ProcessMonitor.NAME)
        if process_service.is_process_alive(self.current_parameters.vmdas_process_name):
            self.status = AgentStatus.RUNNING
        else:
            # self.status == AgentStatus.RUNNING or self.status == AgentStatus.STARTING:
            # no process EK80 but we are marked as RUNNING
            self.status = AgentStatus.STOPPED
        t1_stop = time.perf_counter()
        services.get_supervisor_logger().info(
            f"Compute status for {self.name} (took {t1_stop - start}s)"
        )

        # compute data status model
        #we do not
        self.data_monitor.compute_datastatus(
            datastatus_timeout=self.current_parameters.datastatus_timeout
        )
        self.data_status = self.data_monitor.data_status


    def stop(self):
        try:
            self._stop()
            self.status = AgentStatus.STOPPED

        except Exception as e:
            self.logger.exception(
                f"An exception occured while trying to start ADCP {e}"
            )
            self.status = AgentStatus.FAILURE

    def _stop(self):
        """Stop RDI ADCP associated processes
        These could be the BBTalk like configurator (called from python through cmd_file_reader.py),
        or the vmdas process
        """
        # stop vmdas.exe if it is running
        kill_process(self.current_parameters.vmdas_process_name)
        # We consider that no python.exe is currently running and locking the serial port"""

        # send a cmd to stop pinging
        #do not stop pinging
        #rdi_processes.stop_pinging(current_parameter=self.current_parameters)


    def load_configuration(self, configuration_name: Union[Path, str]):
        """
         Send a new configuration to ADCP,

        - **configuration_name** could be either a keyname or the path of an existing file on disk

        """
        previous_status = self.status
        config_dir = self.working_directory / self.current_parameters.cmd_file_dir
        config_list = list(config_dir.glob("*.txt"))
        config_dict = {v.stem: v for v in config_list}

        file_path = None
        # check if the configuration name is a key of our dictionnary
        if configuration_name in config_dict.keys():
            file_path = config_dict[configuration_name]
        else:
            # try if the value is an existing file somewhere else
            if Path(configuration_name).exists():
                file_path = Path(configuration_name)
        if file_path is None:
            raise FileNotFoundError(f"{file_path} does not exists")

        self.stop()
        rdi_processes.load_configuration(
            filename=file_path,
            working_directory=self.working_directory,
            current_parameter=self.current_parameters,
            rdi_state=self.rdi_state
        )
        #restart vmdas if it was already running
        if previous_status == AgentStatus.RUNNING:
            self.start()

    def declare_api(self, app: FastAPI):
        router = APIRouter(prefix="/" + self.agent_id)

        @router.get(f"/{paths.rdi_config_id}")
        def get_config_files():
            """
            Retrieve the list of avalaible predefined configuration

            Configuration files are retrieved from adcp working directory
            """
            config_dir = self.working_directory / self.current_parameters.cmd_file_dir
            files = list(config_dir.glob("*.txt"))
            config_dict = {v.stem: v for v in files}
            return {"config_files": config_dict}

        @router.put(f"/{paths.rdi_load_config}")
        def load_configuration(config: ConfigParameter):
            """
            Send a new configuration to ADCP,

            - **configuration_name** could be either a keyname or the path of an existing file on disk
            """
            try:
                self.load_configuration(configuration_name=config.configuration_name)
            except FileNotFoundError as exc:
                config_dir = (
                    self.working_directory / self.current_parameters.cmd_file_dir
                )
                raise HTTPException(
                    status_code=404,
                    detail=f"configuration {config.configuration_name} definition not found (file does neither exist in {config_dir} nor is a regular file)",
                ) from exc

        # now really add route to app
        app.include_router(router)
