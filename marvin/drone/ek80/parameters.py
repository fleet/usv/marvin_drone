from pydantic import BaseModel  # pylint: disable=no-name-in-module


class AgentParameters(BaseModel):
    process_name = "EK80.exe"
    ek80_exe_dir = r"c:\Program Files\Simrad\NGE\EK80\Bin"
    data_directories = [r"E:\EK80\DELMOGES\CW", r"E:\EK80\DELMOGES\FM"]
    data_pattern = ["*.raw"]
    file_monitor_period = 80
    datastatus_timeout = 60 * 10  # if no data was produced or modified since that time, data status is marqued as in error
