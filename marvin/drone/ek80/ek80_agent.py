""""
Manage EK80 software start and stop
To allow software to run properly, adapt and run the following command on the computer
> netsh http add urlacl url=http://+:37655/ user=Administrateur

"""
import os
import subprocess
import time
from pathlib import Path

from fastapi import FastAPI
from pydantic import BaseModel  # pylint: disable=no-name-in-module

from marvin.common import api_path as paths, processes
from marvin.common.agent.agent_status import AgentStatus
from marvin.common.file_watcher import DataStatusMonitor
from marvin.common.processes import kill_process
from marvin.drone.ek80.parameters import AgentParameters
from marvin.common.agent.agent import Agent
from marvin.drone.system import common_logger
from marvin.drone.system.services.process_services import ProcessMonitor
from marvin.drone.system.services.services import Services

LOGGER_NAME = "EK80 Agent"


class EK80Agent(Agent):
    def __init__(self, working_directory: Path, agent_id=paths.rdi_agent_id):
        super().__init__(name="EK80 Software", agent_id=agent_id)
        self.working_directory = working_directory
        self.current_parameters = AgentParameters()
        self.logger = common_logger.configure(
            prefix="EK80_agent", logger_name=LOGGER_NAME
        )
        self.data_monitor = DataStatusMonitor(
            directories=[Path(directory) for directory in self.current_parameters.data_directories],
            filter_pattern=self.current_parameters.data_pattern,
            period_s=self.current_parameters.file_monitor_period,
        )

    def start(self):
        self.status = AgentStatus.STARTING
        try:
            self.logger.info(
                f"Starting {self.current_parameters.ek80_exe_dir}/{self.current_parameters.process_name}"
            )
            # processes.spawn_process did not worked (elevation issue)
            current_dir = os.getcwd()
            os.chdir(self.current_parameters.ek80_exe_dir)
            # os.system(self.current_parameters.process_name) #Freeze, use popen instead
            subprocess.Popen(
                [self.current_parameters.process_name], shell=True
            )  # pylint: disable=consider-using-with
            os.chdir(current_dir)

        except Exception as e:
            self.logger.exception(
                f"An exception occured while trying to start agent {e}"
            )
        # start EK80 output file monitoring

    def stop(self):
        try:
            self.logger.info(f"Stopping {self.current_parameters.process_name}")
            self._stop()
            self.status = AgentStatus.STOPPED

        except Exception as e:
            self.logger.exception(
                f"An exception occured while trying to start {self.name} {e}"
            )
            self.status = AgentStatus.FAILURE

    def _stop(self):
        """
        EK80 processes
        """
        # stop ek80.exe if it is running
        kill_process(self.current_parameters.process_name)

    def compute_model(self, services: Services):
        """
        Function called periodically in the main loop to ensure heavy weight status retrieval and computations
        """
        start = time.perf_counter()
        process_service = services.get_service(ProcessMonitor.NAME)
        if process_service.is_process_alive(self.current_parameters.process_name):
            self.status = AgentStatus.RUNNING
        else:
            # self.status == AgentStatus.RUNNING or self.status == AgentStatus.STARTING:
            # no process EK80 but we are marked as RUNNING
            self.status = AgentStatus.STOPPED
        t1_stop = time.perf_counter()
        services.get_supervisor_logger().info(
            f"Compute status for {self.name} (took {t1_stop-start}s)"
        )

        # compute data status model
        self.data_monitor.compute_datastatus(
            datastatus_timeout=self.current_parameters.datastatus_timeout
        )
        self.data_status = self.data_monitor.data_status
